Paddle = Class{}

local PADDLE_SPEED = 200



function Paddle:init (x,y,w,h)
	self.x = x
	self.y = y
	self.width = w
	self.height = h
	self.dy = 0
	
	self.score = 0
	
	self.functions = {
		shaking = self.ai1,
		middle = self.ai_middle,
		edge = self.ai_edge,
		edgeBck = self.ai_edgeBck,
		edgeFwd = self.ai_edgeFwd,
	}
	self.names = getKeys(self.functions)	-- no len!!!
	self.len = #self.names
	self.aiNum = 0
	self.aiName = ''
	self.aiFunction = nil		
end


function Paddle:update (dt)

	if self.dy == 0 then
		-- do nothing, most of the time
	elseif self.dy < 0 then
		self.y = math.max(0, self.y + self.dy * dt)
	else
		self.y = math.min(VIRTUAL_HEIGHT - self.height, self.y + self.dy * dt)
	end
end
function Paddle:up()
	self.dy = -PADDLE_SPEED
end
function Paddle:down()
	self.dy = PADDLE_SPEED
end
function Paddle:stop()
	self.dy = 0
end

function Paddle:render ()
	love.graphics.setColor(255, 255, 255, 255)
	love.graphics.rectangle('fill', self.x, self.y, self.width, self.height)
end

function Paddle:toggleAi ()
	self.aiNum = (self.aiNum + 1) % (self.len + 1)
	self.aiName = self.names[self.aiNum] and self.names[self.aiNum] or ''
	self.aiFunction = self.functions[self.aiName]
end

function Paddle:runAi (b)
	if self.aiFunction then self:aiFunction(b) end
end



-- Ai functions



-- sense 


-- sense ball direction, edge up
function Paddle:ai_edgeFwd (b)

	if b.dy > 0 then		-- ball down b.dy +
		
		if self.y < b.y and self.y + self.height > b.y + b.height then
			-- do nothing
		elseif self.y + b.height < b.y then
			self:down()		-- radi ok
		elseif self.y + self.height - b.height > b.y + b.height then
			self:up()	-- bug
		end
	elseif b.dy < 0 then	-- ball up b.dy -
		if self.y < b.y and self.y + self.height > b.y + b.height then
			-- do nothing
		elseif self.y + self.height - b.height > b.y + b.height then
			self:up()
		end--else
		if self.y + b.height < b.y then
			self:down()
		end
	end
end

-- ok, about middle catch
function Paddle:ai_middle (b)
	
	if (self.y + self.height/4) > b.y then
		self:up()
	elseif (self.y + self.height*3/4) < b.y + b.height then
		self:down()
	else
		self:stop()
	end

end

-- ok, edge catching
function Paddle:ai_edge (b)	

	if self.y > b.y then
		self:up()
	elseif self.y + self.height < b.y + b.height then
		self:down()
	else
		self:stop()
	end
end

-- edge 2
function Paddle:ai_edgeBck (b)
	if self.y + self.height > b.y + b.height and self.y < b.y then
		self:stop()
	
	elseif self.y < b.y then
		self:down()
	elseif self.y + self.height > b.y + b.height then
		self:up()
	end
end

-- bad ai
-- shaking because always going up/down. paddle is larger than ball
function Paddle:ai1 (b)
	if self.y + self.height > b.y + b.height then
		self:up()
	elseif self.y < b.y then
		self:down()
	else
		self:stop()
	end
end