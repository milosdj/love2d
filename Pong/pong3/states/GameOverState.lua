GameOverState = Class{__includes = BaseState}


function GameOverState:update (dt)

	ball:reset()


	-- decide serving player as the opposite of who won
	if winningPlayer == 1 then
		servingPlayer = 2
	else
		servingPlayer = 1
	end

	if keyDown['enter'] or keyDown['return'] then
		player1.score = 0
		player2.score = 0
		keyDown['enter'] = false
		keyDown['return'] = false
		gStateMachine:change('serve')
	end
end


function GameOverState:render ()

	love.graphics.setFont(largeFont)
	love.graphics.printf('Player '..winningPlayer..' wins!', 0, 10, VIRTUAL_WIDTH, 'center')
	love.graphics.setFont(smallFont)
	love.graphics.printf('Press Enter to restart!', 0, 30, VIRTUAL_WIDTH, 'center')
	displayScore()

end