ServeState = Class{__includes = BaseState}

function ServeState:update (dt)
	
	ball.dy = math.random(-50, 50)
	if servingPlayer == 1 then
		ball.dx = math.random(140, 200)
	else
		ball.dx = -math.random(140, 200)
	end	
	
	if keyDown['enter'] or keyDown['return'] then
		gStateMachine:change('play')
	end
	
	-- move paddle in serve state
	-- player 1 movement
	if keyDown['w'] then
		player1:up()
	elseif keyDown['s'] then
		player1:down()
	else
		player1:stop()
	end

	-- player 2 movement
	if keyDown['up'] then
		player2:up()
	elseif keyDown['down'] then
		player2:down()
	else
		player2:stop()
	end	
	
	player1:update(dt)
	player2:update(dt)
	
end

function ServeState:render()

	player1:render()
	player2:render()
	ball:render()

	love.graphics.setFont(smallFont)
	love.graphics.setColor(255,255,255,255)
	love.graphics.printf('Player '..servingPlayer.."'s serve!", 0, 10, VIRTUAL_WIDTH, 'center')
	love.graphics.printf('Press Enter to serve!', 0, 20, VIRTUAL_WIDTH, 'center')
	
	displayScore()
end