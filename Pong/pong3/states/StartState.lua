StartState = Class{__includes = BaseState}

function StartState:update (dt)
	
	if keyDown['enter'] or keyDown['return'] then
		keyDown['enter'] = false
		keyDown['return'] = false
		gStateMachine:change('serve')
	end
end

function StartState:render()

	love.graphics.setColor(255,255,255,255)
local str = [[

Welcome to Pong!
	
f - toggle FPS

1 - toggle AI1 (left player)

2 - toggle AI2 (right player)

Player 1	w/s,
Player 2	up/down

	
Press Enter to begin!
]]
	love.graphics.printf(str, 0, 10, VIRTUAL_WIDTH, 'center')	
end