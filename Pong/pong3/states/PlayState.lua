PlayState = Class{__includes = BaseState}

local SPEED_UP = 1.03

function PlayState:init()

end


function PlayState:update (dt)

	ball:update(dt)
	
  -- player 1 movement
	if keyDown['w'] then
		player1:up()
	elseif keyDown['s'] then
		player1:down()
	else
		player1:stop()
	end

	-- player 2 movement
	if keyDown['up'] then
		player2:up()
	elseif keyDown['down'] then
		player2:down()
	else
		player2:stop()
	end
	
	player1:runAi(ball)
	player2:runAi(ball)
	
	player1:update(dt)
	player2:update(dt)


	-- score 2
	if ball.x < 0 then
		servingPlayer = 1
		player2.score = player2.score + 1
		sounds.score:play()
			
		if player2.score >= 10 then
			gStateMachine:change('gameOver')
			winningPlayer = 2
		else
			ball:reset()
			gStateMachine:change('serve')
		end
	end
		
		-- score 1
	if ball.x > VIRTUAL_WIDTH then
		servingPlayer = 2
		player1.score = player1.score + 1
		sounds.score:play()
		
		if player1.score >= 10 then
			winningPlayer = 1
			gStateMachine:change('gameOver')
		else
			gStateMachine:change('serve')
			ball:reset()
		end
	end
	

	if ball:collides(player1) then		-- left paddle
		sounds.paddle_hit:play()
		ball.dx = -ball.dx * SPEED_UP
		ball.x = player1.x + player1.width

		-- keep velocity going in the same direction, but randomize it
		if ball.dy < 0 then
			ball.dy = -math.random(10, 150)
		else
			ball.dy = math.random(10, 150)
		end
	end
	
	if ball:collides(player2) then		-- right paddle
		sounds.paddle_hit:play()
		ball.dx = -ball.dx * SPEED_UP
		ball.x = player2.x - ball.width

		-- keep velocity going in the same direction, but randomize it
		if ball.dy < 0 then
			ball.dy = -math.random(10, 150)
		else
			ball.dy = math.random(10, 150)
		end
	end

	-- upper screen boundary collision and reverse if collided
	if ball.y <= 0 then
		sounds.wall_hit:play()
		ball.y = 0
		ball.dy = -ball.dy
	end
	-- lower screen boundary
	if ball.y >= VIRTUAL_HEIGHT - ball.height then
		sounds.wall_hit:play()
		ball.y = VIRTUAL_HEIGHT - ball.height
		ball.dy = -ball.dy
	end
end
	



function PlayState:render()
	player1:render()
	player2:render()
	ball:render()
end