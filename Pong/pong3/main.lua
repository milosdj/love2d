-- github.com/ulydev/push
push = require('lib/push')

-- github.com/vrld/hump/blob/master/class.lua
Class = require 'lib/class'

require 'Paddle'
require 'Ball'

require 'StateMachine'
require 'states/BaseState'
require 'states/StartState'
require 'states/ServeState'
require 'states/PlayState'
require 'states/GameOverState'


WINDOW_WIDTH = 1280
WINDOW_HEIGHT = 720

VIRTUAL_WIDTH = 432
VIRTUAL_HEIGHT = 243


FPS = false

keyDown = {}

-- init state
function love.load ()
	love.graphics.setDefaultFilter('nearest', 'nearest')
	love.window.setTitle('Pong')
	math.randomseed(os.time())

	smallFont = love.graphics.newFont('img/font.ttf', 8)
	largeFont = love.graphics.newFont('img/font.ttf', 16)
	scoreFont = love.graphics.newFont('img/font.ttf', 32)
	love.graphics.setFont(smallFont)
	
	sounds = {
		paddle_hit = love.audio.newSource('snd/paddle_hit.wav', 'static'),
		score = love.audio.newSource('snd/score.wav', 'static'),
		wall_hit = love.audio.newSource('snd/wall_hit.wav', 'static')
	}
	
	
	push:setupScreen(VIRTUAL_WIDTH, VIRTUAL_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT,
	{fullscreen=false, resizable=true, vsync=true})
	
	servingPlayer = math.random(1,2)
	winningPlayer = 0

	
	player1 = Paddle(10, 30, 5, 20)
	player2 = Paddle(VIRTUAL_WIDTH - 10, VIRTUAL_HEIGHT -30, 5, 20)
	
	ball = Ball(VIRTUAL_WIDTH / 2 - 2, VIRTUAL_HEIGHT / 2 - 2, 4, 4)
	

	gStateMachine = StateMachine {
		start = StartState,
		serve = ServeState,
		play = PlayState,
		gameOver = GameOverState
	}
	
	gStateMachine:change('start')
end

function love.resize(w, h)
	push:resize(w, h)
end


function love.update(dt)
	gStateMachine:update(dt)
end

function love.keyreleased(k)
	keyDown[k] = false
	
	if k == 'escape' then	love.event.quit()
	elseif k == '1' then	player1:toggleAi()
	elseif k == '2' then	player2:toggleAi()
	elseif k == 'f' then	FPS = not FPS		
	end
end

function love.keypressed(key)
	keyDown[key] = true
end


function love.draw ()
push:start()
	love.graphics.clear(40/255, 45/255, 52/255,1)

	displayAI()
	if FPS then displayFPS() end
	
	gStateMachine:render()
	
push:finish()
end


function displayFPS ()
	
	love.graphics.setFont(smallFont)
	love.graphics.setColor(0,255,0,255)
	love.graphics.print('FPS: ' .. love.timer.getFPS(), 10, 1)
end

function displayAI ()
	love.graphics.setFont(smallFont)
	love.graphics.setColor(255,255,0,255)
	--love.graphics.print(('AI1: '..player1.aiNum..'-'..player1.aiName..' AI2: '..player2.aiNum..'-'..player2.aiName) , 50, 1)
	local s = ''
	if player1.aiNum ~= 0 then
		s = 'AI1: '..player1.aiName
	end
	if player2.aiNum ~= 0 then
		s = s..'	AI2: '..player2.aiName
	end
	love.graphics.print(s, 50, 1)
end

function displayScore ()
	love.graphics.setFont(scoreFont)
	love.graphics.setColor(255,255,255,255)	
    love.graphics.print(player1.score, VIRTUAL_WIDTH / 2 - 50, VIRTUAL_HEIGHT / 3)
    love.graphics.print(player2.score, VIRTUAL_WIDTH / 2 + 30, VIRTUAL_HEIGHT / 3)
end


function getKeys (t)
	local keyset={}
	local n=0
	
	for k,v in pairs(t) do
		n=n+1
		keyset[n]=k
	end
	return keyset
end

function strTable(t)
	local s = ''
	
	for k, v in pairs(t) do
		s = s .. k..'-'..v..', '
	end
	
	
return s
end