-- github.com/ulydev/push
push = require('push')

-- github.com/vrld/hump/blob/master/class.lua
Class = require 'class'

require 'Paddle'
require 'Ball'

require 'StateMachine'
require 'states/BaseState'




WINDOW_WIDTH = 1280
WINDOW_HEIGHT = 720

VIRTUAL_WIDTH = 432
VIRTUAL_HEIGHT = 243

PADDLE_SPEED = 200
SPEED_UP = 1.03

AI1 = false
AI2 = false
FPS = false

--ball = {}


-- init state
function love.load ()
	love.graphics.setDefaultFilter('nearest', 'nearest')
	love.window.setTitle('Pong')
	math.randomseed(os.time())

	smallFont = love.graphics.newFont('font.ttf', 8)
	largeFont = love.graphics.newFont('font.ttf', 16)
	scoreFont = love.graphics.newFont('font.ttf', 32)
	love.graphics.setFont(smallFont)
	
	sounds = {
		paddle_hit = love.audio.newSource('sounds/paddle_hit.wav', 'static'),
		score = love.audio.newSource('sounds/score.wav', 'static'),
		wall_hit = love.audio.newSource('sounds/wall_hit.wav', 'static')
	}
	
	
	push:setupScreen(VIRTUAL_WIDTH, VIRTUAL_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT,
	{fullscreen=false, resizable=true, vsync=true})
	
	servingPlayer = math.random(1,2)
	winningPlayer = 0

	player1Score = 0
	player2Score = 0
	
	player1 = Paddle(10, 30, 5, 20)
	player2 = Paddle(VIRTUAL_WIDTH - 10, VIRTUAL_HEIGHT -30, 5, 20)
	
	ball = Ball(VIRTUAL_WIDTH / 2 - 2, VIRTUAL_HEIGHT / 2 - 2, 4, 4)
	

	gameState = 'start'
end

function love.resize(w, h)
	push:resize(w, h)
end


function love.update(dt)
	
   -- player 1 movement
	if love.keyboard.isDown('w') then
		player1.dy = -PADDLE_SPEED
	elseif love.keyboard.isDown('s') then
		player1.dy = PADDLE_SPEED
	else
		player1.dy = 0
	end

	-- player 2 movement
	if love.keyboard.isDown('up') then
		player2.dy = -PADDLE_SPEED
	elseif love.keyboard.isDown('down') then
		player2.dy = PADDLE_SPEED
	else
		player2.dy = 0
	end
	
	if AI1 then
		player1:ai(ball)
	end
	
	if AI2 then
		player2:ai(ball)
	end
	

	player1:update(dt)
	player2:update(dt)


    if gameState == 'serve' then
	
        ball.dy = math.random(-50, 50)
        if servingPlayer == 1 then
            ball.dx = math.random(140, 200)
        else
            ball.dx = -math.random(140, 200)
        end
		
    elseif gameState == 'play' then	
	
		ball:update(dt)
		
		-- score 2
		if ball.x < 0 then
			servingPlayer = 1
			player2Score = player2Score + 1
			sounds.score:play()
			
			if player2Score == 10 then
				winningPlayer = 2
				gameState = 'done'
			else
				gameState = 'serve'
				ball:reset()
				gameState = 'serve'
			end
		end
		
		-- score 1
		if ball.x > VIRTUAL_WIDTH then
			servingPlayer = 2
			player1Score = player1Score + 1
			sounds.score:play()
			
			if player1Score == 10 then
				winningPlayer = 1
				gameState = 'done'
			else
				gameState = 'serve'
				ball:reset()
			end
		end
		

		if ball:collides(player1) then		-- left paddle
			sounds.paddle_hit:play()
			ball.dx = -ball.dx * SPEED_UP
			ball.x = player1.x + player1.width

			-- keep velocity going in the same direction, but randomize it
			if ball.dy < 0 then
				ball.dy = -math.random(10, 150)
			else
				ball.dy = math.random(10, 150)
			end
		end
		
		if ball:collides(player2) then		-- right paddle
			sounds.paddle_hit:play()
			ball.dx = -ball.dx * SPEED_UP
			ball.x = player2.x - ball.width

			-- keep velocity going in the same direction, but randomize it
			if ball.dy < 0 then
				ball.dy = -math.random(10, 150)
			else
				ball.dy = math.random(10, 150)
			end
		end

		-- upper screen boundary collision and reverse if collided
		if ball.y <= 0 then
			sounds.wall_hit:play()
			ball.y = 0
			ball.dy = -ball.dy
		end
		-- lower screen boundary
		if ball.y >= VIRTUAL_HEIGHT - ball.height then
			sounds.wall_hit:play()
			ball.y = VIRTUAL_HEIGHT - ball.height
			ball.dy = -ball.dy
		end
	end
	
end


function love.keypressed(key)
	if key == 'escape' then

		love.event.quit()
		
	elseif key == '1' then

		AI1 = not AI1
		
	elseif key == '2' then
	
		AI2 = not AI2
		
	elseif key == 'f' then
	
		FPS = not FPS
		
	elseif key == 'enter' or key == 'return' then
	
		if gameState == 'start' then
			gameState = 'serve'
		elseif gameState == 'serve' then
			gameState = 'play'
        elseif gameState == 'done' then
            gameState = 'serve'

            ball:reset()

            player1Score = 0
            player2Score = 0

            -- decide serving player as the opposite of who won
            if winningPlayer == 1 then
                servingPlayer = 2
            else
                servingPlayer = 1
            end
		end
	end
end


function love.draw ()
	push:start()
	love.graphics.clear(40/255, 45/255, 52/255,1)

	displayAI()
	
    if gameState == 'start' then
		displayStart()
    elseif gameState == 'serve' then
        love.graphics.setFont(smallFont)
        love.graphics.printf('Player '..servingPlayer.."'s serve!", 0, 10, VIRTUAL_WIDTH, 'center')
        love.graphics.printf('Press Enter to serve!', 0, 20, VIRTUAL_WIDTH, 'center')
		displayScore()
    elseif gameState == 'play' then
        -- no UI messages to display in play
    elseif gameState == 'done' then
        -- UI messages
        love.graphics.setFont(largeFont)
        love.graphics.printf('Player '..winningPlayer..' wins!', 0, 10, VIRTUAL_WIDTH, 'center')
        love.graphics.setFont(smallFont)
        love.graphics.printf('Press Enter to restart!', 0, 30, VIRTUAL_WIDTH, 'center')
		displayScore()
    end
	
	
	
	player1:render()
	player2:render()
	ball:render()

	if FPS then displayFPS() end
	
	push:finish()
end


function displayFPS ()
	love.graphics.setFont(smallFont)
	love.graphics.setColor(0,255,0,255)
	love.graphics.print('FPS: ' .. love.timer.getFPS(), 10, 1)
end

function displayAI ()
	love.graphics.setFont(smallFont)
	love.graphics.setColor(255,255,0,255)	
	love.graphics.print((AI1 and 'AI1' or '')..(AI2 and ' AI2' or '') , 50, 1)
	love.graphics.setColor(255,255,255,255)	
	
end

function displayScore()
    love.graphics.setFont(scoreFont)
	love.graphics.setColor(255,255,255,255)	
    love.graphics.print(player1Score, VIRTUAL_WIDTH / 2 - 50, VIRTUAL_HEIGHT / 3)
    love.graphics.print(player2Score, VIRTUAL_WIDTH / 2 + 30, VIRTUAL_HEIGHT / 3)
end

function displayStart()
	love.graphics.setFont(smallFont)
	love.graphics.setColor(255,255,255,255)	

	local str = [[

Welcome to Pong!
	
f - toggle FPS

1 - toggle AI1 (left player)

2 - toggle AI2 (right player)

	
Press Enter to begin!
]]
	love.graphics.printf(str, 0, 10, VIRTUAL_WIDTH, 'center')
end

