TitleScreenState = Class{__includes = BaseState}

function TitleScreenState:update (dt)
	if love.keyboard.wasPressed('enter') or love.keyboard.wasPressed('return')
		gStateMachine:change('play')
	end
end

function TitleScreenState:render()
	love.graphics.setfont(10)
	love.graphics.printf('screen', 0, 64, VIRTUAL_WIDTH, 'center')
	
	love.graphics.setFont(20)
	love.graphics.printf('press enter', 0, 100, VIRTUAL_WIDTH, 'center')
end