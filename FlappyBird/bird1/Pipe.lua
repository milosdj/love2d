Pipe = Class{}

local PIPE_IMAGE = love.graphics.newImage('img/pipe.png')

PIPE_HEIGHT = PIPE_IMAGE:getHeight()
PIPE_WIDTH = PIPE_IMAGE:getWidth()


function Pipe:init(y, flip)

	self.x = VIRTUAL_WIDTH
	
	--math.random(VIRTUAL_HEIGHT / 4, VIRTUAL_HEIGHT - 10)
	self.y = flip and (y ) or y

--	self.width = PIPE_IMAGE:getWidth()
--	self.height = PIPE_IMAGE:getHeight()
--	self.f = flip == 'flip' and true or false
	
	self.scaleY = flip and -1 or 1	-- y scale, -1 mirror
	if flip then
	print('y: '..self.y..' scaleY: '..self.scaleY..(flip and ' flip ' or ''))
	end
end


function Pipe:update(dt)
--	self.x = self.x + PIPE_SPEED * dt
--	self.x = self.x + GROUND_SCROLL_SPEED * dt
end


function Pipe:render()
	love.graphics.draw(PIPE_IMAGE, self.x, self.y, 
	0,	--rotation
	1,	-- x scale, 1 no scale
	self.scaleY
	)
	
	
	
end







