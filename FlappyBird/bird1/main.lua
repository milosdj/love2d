push = require 'push'
Class = require 'class'

require 'Bird'
require 'Pipe'
require 'PipePair'


WINDOW_WIDTH = 1280
WINDOW_HEIGHT = 720

VIRTUAL_WIDTH = 512
VIRTUAL_HEIGHT = 288

GRAVITY = 20

keyDown = {}

BACKGROUND_SPEED = 30
GROUND_SPEED = 60

GAP_HEIGHT = 90


local BACKGROUND_LOOPING_POINT = 413

local background = love.graphics.newImage('img/background.png')
local backgroundScroll = 0
local ground = love.graphics.newImage('img/ground.png')
local groundHeight = ground:getHeight()
local groundScroll = 0

local spawnTimer = 0		-- spawn pipes


local bird = Bird('img/bird.png')
-- local pipe = Pipe()



local pipePairs = {}

-- limit randomness, top screen
local lastY = VIRTUAL_WIDTH / 2 + math.random(-40, 40) - GAP_HEIGHT


local scrolling = true



function love.load()
	love.graphics.setDefaultFilter('nearest', 'nearest')
	love.window.setTitle('Bird')
	math.randomseed(os.time())
	
	push:setupScreen(VIRTUAL_WIDTH, VIRTUAL_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT,
	{vsync = true, fullscreen = false, resizable = true})
	
	
	--love.keyboard.keysPressed = {}
	
end

function resize(w,h)
	push:resize(w,h)
end

function love.keyreleased(k)
	keyDown[k] = false
end
-- function love.isDown('escape')

function love.keypressed(k)

	keyDown[k] = true

	if k == 'escape' then
		love.event.quit()
	end


end

function love.keyboard.wasPressed(k)
	return keyDown[k]
end





function love.update (dt)

if scrolling then

	-- parallax scrolling
	backgroundScroll = (backgroundScroll + BACKGROUND_SPEED * dt) % BACKGROUND_LOOPING_POINT
	groundScroll = (groundScroll + GROUND_SPEED * dt) % VIRTUAL_WIDTH




	spawnTimer = spawnTimer + dt
	
	if spawnTimer > 2 then
		
		local y = math.max(20, -- min od vrha ekrana
		math.min(lastY + math.random(-40, 40), VIRTUAL_HEIGHT - GAP_HEIGHT -groundHeight - 20))	-- -90 min od dna
		
		lastY = y
		table.insert(pipePairs, PipePair(y))
		spawnTimer = 0
	end
	
	bird:update(dt)
	
	for k, pair in pairs(pipePairs) do
		pair:update(dt)
		
		if bird:collides(pair.pipe_up) -- or
		--bird:collides(pair.pipe_down)
		then
			scrolling = false
		end
		
		if pair.x < -PIPE_WIDTH then
			pair.remove = true
		end
		
	end
		
	for k, pair in pairs(pipePairs) do
		if pair.remove then table.remove(pipePairs, k) end
	end

end -- end if scrolling
	
	--love.keyboard.keysPressed = {}
end



function love.draw()
push:start()

	-- parallax scrolling, image at least x2
	love.graphics.draw(background, -backgroundScroll, 0)
	
	for k, pair in pairs(pipePairs) do
		pair:render()
	end
	
	love.graphics.draw(ground, -groundScroll, VIRTUAL_HEIGHT - ground:getHeight())
	
	
	bird:render()

	
	
push:finish()
end











