push = require 'push'
Class = require 'class'

require 'Bird'
require 'Pipe'
require 'PipePair'
require 'StateMachine'

require 'state/BaseState'
require 'state/PlayState'
require 'state/TitleScreenState'
require 'state/ScoreState'
require 'state/CountdownState'



WINDOW_WIDTH = 1280
WINDOW_HEIGHT = 720

VIRTUAL_WIDTH = 512
VIRTUAL_HEIGHT = 288

GRAVITY = 10

keyDown = {}
mouseDown = {}


BACKGROUND_SPEED = 30
GROUND_SPEED = 60



local background = love.graphics.newImage('img/background.png')
local backgroundScroll = 0
local BACKGROUND_LOOPING_POINT = 413

local ground = love.graphics.newImage('img/ground.png')
local groundScroll = 0
GROUND_HEIGHT = ground:getHeight()
groundY = VIRTUAL_HEIGHT - GROUND_HEIGHT

gMUSIC = false
gPAUSE = false




function love.load()
	love.graphics.setDefaultFilter('nearest', 'nearest')
	love.window.setTitle('Bird')
	math.randomseed(os.time())
	
	smallFont = love.graphics.newFont('img/font.ttf', 8)
	mediumFont = love.graphics.newFont('img/flappy.ttf', 14)
	flappyFont = love.graphics.newFont('img/flappy.ttf', 28)
	hugeFont = love.graphics.newFont('img/flappy.ttf', 56)
	love.graphics.setFont(flappyFont)
	
	sounds = {
		jump = love.audio.newSource('audio/jump.wav', 'static'),
		explosion = love.audio.newSource('audio/explosion.wav', 'static'),
		hurt = love.audio.newSource('audio/hurt.wav', 'static'),
		score = love.audio.newSource('audio/score.wav', 'static'),

		-- https://freesound.org/people/xsgianni/sounds/388079/
		music = love.audio.newSource('audio/marios_way.mp3', 'static')
	}
	
	sounds['music']:setLooping(true)
	--sounds['music']:play()
	
	
	
	
	
	push:setupScreen(VIRTUAL_WIDTH, VIRTUAL_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT,
	{vsync = true, fullscreen = false, resizable = true})
	
	
	gStateMachine = StateMachine {
		--['title'] = function () return TitleScreenState() end,	???
		title = TitleScreenState,
		countdown = CountdownState,
		play = PlayState,
		score = ScoreState,
		--['play'] = function () return PlayState() end,
	}
	gStateMachine:change('title')
	
end

function resize(w,h)
	push:resize(w,h)
end

function love.keyreleased(k)
	keyDown[k] = false
-- function love.isDown('escape')
	if k == 'escape' then
		love.event.quit()
	elseif k == 'm' then
		if gMUSIC then
			gMUSIC = false
			sounds['music']:stop()
		else
			gMUSIC = true
			sounds['music']:play()
		end
	elseif k == 'p' and gStateMachine.name == 'play' then
		if gPAUSE then
			gPAUSE = false
		else
			gPAUSE = true
		end
	end
end

function love.keypressed(k)
	keyDown[k] = true
end

function love.mousepressed(x, y, button)
	mouseDown[button] = true
end

function love.mousereleased(x, y, button)
	mouseDown[button] = false
end


function love.update (dt)

	if gPAUSE then
		sounds['music']:stop()
	else
		if gMUSIC then sounds['music']:play() end
		-- parallax scrolling
		backgroundScroll = (backgroundScroll + BACKGROUND_SPEED * dt) % BACKGROUND_LOOPING_POINT
		groundScroll = (groundScroll + GROUND_SPEED * dt) % VIRTUAL_WIDTH

		gStateMachine:update(dt)
	end
end



function love.draw()
push:start()

	-- parallax scrolling, image at least x2
	love.graphics.draw(background, -backgroundScroll, 0)
	
	gStateMachine:render()
	
	love.graphics.draw(ground, -groundScroll, groundY)
	
	if gPAUSE then
		love.graphics.print('PAUSE', VIRTUAL_HEIGHT/2, VIRTUAL_WIDTH/2)
	end
	
push:finish()
end











