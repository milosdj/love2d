Pipe = Class{}

local PIPE_IMAGE = love.graphics.newImage('img/pipe.png')

PIPE_HEIGHT = PIPE_IMAGE:getHeight()
PIPE_WIDTH = PIPE_IMAGE:getWidth()


function Pipe:init(y, flip)

	self.x = VIRTUAL_WIDTH
	
	--math.random(VIRTUAL_HEIGHT / 4, VIRTUAL_HEIGHT - 10)
	self.y = math.floor(y)

	self.width = PIPE_IMAGE:getWidth()
	self.height = PIPE_IMAGE:getHeight()
	
	self.flip = false
	self.scaleY = 1		-- normal scale
	self.offsetY = 0	-- no Y offset
	
	if flip then
		self.flip = true
		self.scaleY = -1 -- mirror Y
		self.offsetY = self.height		-- return x,y to 0, 0
--		print('y: '..self.y..' scaleY: '..self.scaleY..' flip')
	else
--		print('y: '..self.y)
	end
end


function Pipe:update(dt)
--	self.x = self.x + PIPE_SPEED * dt
--	self.x = self.x + GROUND_SCROLL_SPEED * dt
end


function Pipe:render()
	love.graphics.draw(PIPE_IMAGE, self.x, self.y, 
	0,				--rotation
	1,				-- x scale, 1 no scale
	self.scaleY,
	0,				-- x offset
	self.offsetY	-- y offset
	)
	
end