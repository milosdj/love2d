PipePair = Class{}


function PipePair:init (midY)

	self.x = VIRTUAL_WIDTH + 2		-- right offscreen offset for new pipe

	self.gapHeight = 90 + math.random(-30, 30)
	self.gap2 = math.floor(self.gapHeight/2)
	
--	midY = (midY > 0 and midY < VIRTUAL_HEIGHT )and midY or VIRTUAL_HEIGHT/2
		-- limit randomness, top screen	-- donja cev
--		self.y = math.max(10 + self.gapHeight, -- min od vrha ekrana
--		math.min(lastY + math.random(-self.gapHeight/3, self.gapHeight/3), groundY - 10))	-- - min od dna
	self.y = math.max(0, -- min od vrha ekrana
		math.min(midY + math.random(-self.gap2, self.gap2),
			groundY))	-- - min od dna
	self.y = math.floor(self.y)		-- middle gap point
	
	
	self.pipe_up = Pipe(self.y - self.gap2 - PIPE_HEIGHT, 'flip')
	self.pipe_down = Pipe(self.y + self.gap2)

	self.remove = false
	self.scored = false
end


function PipePair:update(dt)

	if self.x > -PIPE_WIDTH then
	
		self.x = self.x - (GROUND_SPEED * dt)
		self.pipe_up.x = self.x
		self.pipe_down.x = self.x
	else
		self.remove = true
	end
	
end


function PipePair:render ()
	self.pipe_up:render()
	self.pipe_down:render()
end

