PlayState = Class{__includes = BaseState}


function PlayState:init ()

	self.bird = Bird('img/bird.png')
	self.pipePairs = {}
	self.removePipe = false
	self.spawnTimer = 0
	self.spawnTime = self.rnd()
	
	self.score = 0
	
	self.midY = VIRTUAL_HEIGHT / 2
end


function PlayState:rnd()
	--return 3 + (math.random() * math.random(-1, 1))
	return 1.75 + math.random()
end


function PlayState:update (dt)

	self.bird:update(dt)
	
	self.spawnTimer = self.spawnTimer + dt
	
	if self.spawnTimer > self.spawnTime then
		self.spawnTimer = 0
		self.spawnTime = self.rnd()
		
		local pp = PipePair(self.midY)
		self.midY = pp.y
		table.insert(self.pipePairs, pp)
	end
	
	-- optimize to not iterate thru all pipes
	for k, pair in pairs(self.pipePairs) do
		pair:update(dt)
		
		if not pair.scored then
			if pair.x + PIPE_WIDTH < self.bird.x then
				self.score = self.score + math.ceil(10/(70/ pair.gapHeight))
				pair.scored = true
				sounds['score']:play()
			end
		end		
		
		if self.bird:collides(pair.pipe_up) or
		self.bird:collides(pair.pipe_down)
		then
			sounds['explosion']:play()
			sounds['hurt']:play()
--			gStateMachine:change('title')
			gStateMachine:change('score', {score = self.score})
			break
		end
		
		if pair.x < -PIPE_WIDTH then
			pair.remove = true
			self.removePipe = true
		end
		
	end
	
	
	if self.removePipe then	
		self.removePipe = false
		for k, pair in pairs(self.pipePairs) do
			if pair.remove then
				table.remove(self.pipePairs, k) 
				break
			end
		end
	end

	
	-- ground
	if self.bird.y + self.bird.height +1 > groundY then
		self.bird.y = groundY - self.bird.height -1
		self.score = self.score + 1
	--	sounds['jump']:play()
		sounds['score']:play()
		sounds['hurt']:play()
		--gStateMachine:change('title')
	end	
	
	

end



function PlayState:render ()

	for k, pair in pairs(self.pipePairs) do
		pair:render()
	end
	
	self.bird:render()
	
	love.graphics.setFont(flappyFont)
	love.graphics.print('Score: '..self.score, 8, 8)
end



