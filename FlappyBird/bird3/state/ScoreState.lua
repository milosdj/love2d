ScoreState =  Class{__includes = BaseState}

function ScoreState:enter(params)
	self.score = params.score
	-- 400 x 220
	self.medals = love.graphics.newImage('img/medals.png')
	self.medalWidth = self.medals:getWidth()
	self.medalHeight = self.medals:getHeight()
	
	-- x,y, w, h, ref w, ref h
	self.qGold = love.graphics.newQuad(0, 0, 120, 220, self.medalWidth, self.medalHeight)
	self.qSilver = love.graphics.newQuad(135, 0, 120, 220, self.medalWidth, self.medalHeight)
	self.qBronze = love.graphics.newQuad(280, 0, 120, 220, self.medalWidth, self.medalHeight)
	self.quad = love.graphics.newQuad(0, 0, 1, 1, self.medalWidth, self.medalHeight)
	
end

function ScoreState:update (dt)

	if keyDown['enter'] or keyDown['return'] then
		gStateMachine:change('countdown')
	end
end


function ScoreState:render ()

	if self.score >= 1000 then
		self.quad = self.qGold
	elseif self.score >= 500 then
		self.quad = self.qSilver
	elseif self.score >= 250 then
		self.quad = self.qBronze
	end
--	self.quad = self.qGold
	love.graphics.draw(self.medals, self.quad,
	VIRTUAL_WIDTH/2 - 60,
	0)

	love.graphics.setFont(flappyFont)
	love.graphics.printf('Aaaa... You lost!', 0, 30, VIRTUAL_WIDTH, 'center')
	
	love.graphics.setFont(mediumFont)
	love.graphics.printf('Score: '..self.score, 0, 120, VIRTUAL_WIDTH, 'center')
	
	love.graphics.printf('Pres Enter to play again!', 0, 250, VIRTUAL_WIDTH, 'center')



end