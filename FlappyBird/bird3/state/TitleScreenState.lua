TitleScreenState = Class{__includes = BaseState}

function TitleScreenState:update (dt)

	if keyDown['enter'] or keyDown['return'] then
		gStateMachine:change('countdown')
	end
end

function TitleScreenState:render ()
	love.graphics.setFont(flappyFont)
	love.graphics.printf('FlappyBird', 0, 64, VIRTUAL_WIDTH, 'center')
	
	love.graphics.setFont(mediumFont)
	love.graphics.printf('Press Enter', 9, 100, VIRTUAL_WIDTH, 'center')
	
	
	love.graphics.setFont(smallFont)
	local str = [[
ESC - quit game
M	-	music

ENTER - start game
P - pause

SPACE/MOUSE Button	-	bird up
]]
	love.graphics.printf(str, 10, 180, VIRTUAL_WIDTH, 'left')
	
end