PlayState = Class{__includes = BaseState}


function PlayState:init ()

	self.bird = Bird('img/bird.png')
	self.pipePairs = {}
	self.removePipe = false
	self.spawnTimer = 0
	
	self.score = 0
	
	self.lastY = VIRTUAL_HEIGHT / 2 + math.random(-50, 50) + GAP_HEIGHT/2
end


function PlayState:update (dt)

	self.bird:update(dt)
	
	self.spawnTimer = self.spawnTimer + dt
	
	if self.spawnTimer > 2 then
		self.spawnTimer = 0
		
		-- limit randomness, top screen
		local y = math.max(10 + GAP_HEIGHT, -- min od vrha ekrana
		math.min(self.lastY + math.random(-50, 50), groundY - 10))	-- - min od dna
		
		self.lastY = y
		table.insert(self.pipePairs, PipePair(y))
	end
	
	-- optimize to not iterate thru all pipes
	for k, pair in pairs(self.pipePairs) do
		pair:update(dt)
		
		if not pair.scored then
			if pair.x + PIPE_WIDTH < self.bird.x then
				self.score = self.score + 1
				pair.scored = true
				sounds['score']:play()
			end
		end		
		
		if self.bird:collides(pair.pipe_up) or
		self.bird:collides(pair.pipe_down)
		then
			sounds['explosion']:play()
			sounds['hurt']:play()
--			gStateMachine:change('title')
			gStateMachine:change('score', {score = self.score})
			break
		end
		
		if pair.x < -PIPE_WIDTH then
			pair.remove = true
			self.removePipe = true
		end
		
	end
	
	
	if self.removePipe then	
		self.removePipe = false
		for k, pair in pairs(self.pipePairs) do
			if pair.remove then
				table.remove(self.pipePairs, k) 
				break
			end
		end
	end

	
	-- ground
	if self.bird.y > groundY - self.bird.height then
		self.bird.y = groundY - self.bird.height
		self.score = self.score + 1
	--	sounds['jump']:play()
		sounds['score']:play()
		sounds['explosion']:play()
		--gStateMachine:change('title')
	end	
	
	

end



function PlayState:render ()

	for k, pair in pairs(self.pipePairs) do
		pair:render()
	end
	
	self.bird:render()
	
	love.graphics.setFont(flappyFont)
	love.graphics.print('Score: '..self.score, 8, 8)
end



