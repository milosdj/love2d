TitleScreenState = Class{__includes = BaseState}

function TitleScreenState:update (dt)

	if keyDown['enter'] or keyDown['return'] then
		gStateMachine:change('countdown')
	end
end

function TitleScreenState:render ()
	love.graphics.setFont(flappyFont)
	love.graphics.printf('FlappyBird', 0, 64, VIRTUAL_WIDTH, 'center')
	
	love.graphics.setFont(mediumFont)
	love.graphics.printf('Press Enter', 9, 100, VIRTUAL_WIDTH, 'center')
end