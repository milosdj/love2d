PipePair = Class{}



function PipePair:init (y)

	self.x = VIRTUAL_WIDTH + 32		-- right offscreen offset for new pipe

	self.y = y		-- donja cev
	
	self.pipe_up = Pipe(self.y - GAP_HEIGHT - PIPE_HEIGHT, 'flip')
	self.pipe_down = Pipe(self.y)

	self.remove = false
	self.scored = false
end


function PipePair:update(dt)

	if self.x > -PIPE_WIDTH then
	
		self.x = self.x - (GROUND_SPEED * dt)
		self.pipe_up.x = self.x
		self.pipe_down.x = self.x
	else
		self.remove = true
	end
	
end




function PipePair:render ()
	self.pipe_up:render()
	self.pipe_down:render()
end










