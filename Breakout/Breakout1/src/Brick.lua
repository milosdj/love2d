Brick = Class{}

-- tier color
local paletteColors = {
	-- blue
	[0] = {
		['r'] = 99/255,
		['g'] = 155/255,
		['b'] = 255/255
	},
	-- green
	[1] = {
		['r'] = 106/255,
		['g'] = 190/255,
		['b'] = 47/255
	},
	-- red
	[2] = {
		['r'] = 217/255,
		['g'] = 87/255,
		['b'] = 99/255
	},
	-- purple
	[3] = {
		['r'] = 215/255,
		['g'] = 123/255,
		['b'] = 186/255
	},
	-- gold
	[4] = {
		['r'] = 251/255,
		['g'] = 242/255,
		['b'] = 54/255
	},
	-- brown	locked
	[5] = {
		['r'] = 89/255,
		['g'] = 0.15,
		['b'] = 0.15
	}
}

Brick.width = 32
Brick.height = 16
Brick.locked = false

function Brick:init(x, y)
	-- used for coloring and score calculation
	self.tier = 0	-- 1-5
	self.color = 1	-- 1-4
	
	self.x = x
	self.y = y

	-- used to determine whether this brick should be rendered
	self.inPlay = true
	self.emit = false		-- emit particles
	self.powerup = nil
	self.locked = false
	
	-- https://love2d.org/wiki/ParticleSystem
	-- particle system belonging to the brick, emitted on hit
	self.psystem = love.graphics.newParticleSystem(gTextures['particle'], 64)

	self.psystem:setParticleLifetime(0.5, 1)

-- how long the particle system should emit particles (if -1 then it emits particles forever). 	
	-- ParticleSystem:setEmitterLifetime( life )
	-- give it an acceleration of anywhere between X1,Y1 and X2,Y2 (0, 0) and (80, 80) here
	-- gives generally downward 
	self.psystem:setLinearAcceleration(-15, 0, 15, 80)

	-- normal looks natural; uniform clumpy; ellipse
	--self.psystem:setAreaSpread('normal', 10, 10)
	self.psystem:setEmissionArea('normal', 10, 10)
	--ParticleSystem:setEmissionRate(rate)	-- amount of particles emitted per second. 
end


-- hit on brick, changing bricks color or disappear
function Brick:hit()
	self.emit = true

	if O.sfx then
		gSounds['brick_hit_2']:stop()
		gSounds['brick_hit_2']:play()
		gSounds['brick_hit_1']:stop()
		gSounds['brick_hit_1']:play()
	end

	self.psystem:setColors(
		paletteColors[self.color].r,
		paletteColors[self.color].g,
		paletteColors[self.color].b,
		(60 * (self.tier + 1)) /255,
		paletteColors[self.color].r,
		paletteColors[self.color].g,
		paletteColors[self.color].b,
		0
	)
	self.psystem:emit(64)	
	
--	self.inPlay = false

	if self.color == LOCKED_COLOR and self.tier == 1 then	-- locked
		self.emit = false
		if O.paddle.keys > 0 then		-- unlock if key
			self.emit = true
			O.paddle.keys = O.paddle.keys - 1
			self.tier = self.tier - 1	-- unlock brick
			if O.sfx then gSounds['victory']:play() end
		end
		
		return
	end
	
	if self.tier > 0 then	-- other bricks

		if self.color == 0 then
			self.tier = self.tier - 1
			self.color = MAX_COLOR
		else
			self.color = self.color - 1
		end
	
	else	-- if we're in the first tier and the base color, remove brick from play

		if self.color > 0 then
			self.color = self.color - 1
		else
			self.inPlay = false
			if self.powerup then self.powerup.falling = true end
		end
	end
end


function Brick:update (dt)
	if self.emit then
		self.emit = self.psystem:isActive()
		self.psystem:update(dt)
	end
	
	if self.powerup and self.powerup.falling then
		self.powerup:update(dt)
	end
end


function Brick:render()
	if self.inPlay then
		love.graphics.draw(gTextures['main'], 
			-- multiply color by 4 (-1) to get our color offset, then add tier to that
			-- to draw the correct tier and color brick onto the screen
			gFrames['bricks'][1 + (self.color * 4) + self.tier],
			self.x, self.y)
	end
	
	if self.powerup then self.powerup:render() end
	
	if self.emit then 
		love.graphics.draw(self.psystem, self.x + self.width/2, self.y + self.height)
	end
end

