

function displayFPS()
	love.graphics.setFont(gFonts['small'])
	love.graphics.setColor(0, 1, 0, 1)
	love.graphics.print('FPS: ' .. love.timer.getFPS(), 5, 5)
end

function renderLife(l)

	l = l or 0
	-- start of our health rendering
	local x = VIRTUAL_WIDTH - MAX_LIFE * 12
	
	-- render full hearts
	for i = 1, l do
		love.graphics.draw(gTextures['main'], gFrames['hearts'][1], x, 4)
		x = x + 11
	end

	-- render missing empty hearts
	for i = 1, MAX_LIFE - l do
		love.graphics.draw(gTextures['main'], gFrames['hearts'][2], x, 4)
		x = x + 11
	end
end

function renderScore(score)
	love.graphics.setFont(gFonts['small'])
	local str = 'Keys: '..tostring(O.paddle.keys) .. '  Score: '.. tostring(score)
	
	love.graphics.printf(str, 0, 5, VIRTUAL_WIDTH, 'center')
--	love.graphics.print('Score: '.. score, VIRTUAL_WIDTH - 60, 5)
end



--piece out paddles from sprite sheet
function GenerateQuadsPaddles (atlas)
	local x = 0
	local y = 64

	local counter = 1
	local quads = {}
	local w, h = atlas:getDimensions()

	for i = 0, 3 do
		-- smallest
		quads[counter] = love.graphics.newQuad(x, y, 32, 16, w, h)
		counter = counter + 1
		-- medium
		quads[counter] = love.graphics.newQuad(x + 32, y, 64, 16, w, h)
		counter = counter + 1
		-- large
		quads[counter] = love.graphics.newQuad(x + 96, y, 96, 16, w, h)
		counter = counter + 1
		-- huge
		quads[counter] = love.graphics.newQuad(x, y + 16, 128, 16, w, h)
		counter = counter + 1

		-- prepare X and Y for the next set of paddles
		x = 0
		y = y + 32
	end

	return quads
end

-- piece out the balls from sprite sheet
function GenerateQuadsBallsold (atlas)
	local x = 96
	local y = 48

	local w, h = atlas:getDimensions()
	local counter = 1
	local quads = {}

	for i = 0, 3 do
		quads[counter] = love.graphics.newQuad(x, y, 8, 8, w, h)
		x = x + 8
		counter = counter + 1
	end

	x = 96
	y = 56

	for i = 0, 2 do
		quads[counter] = love.graphics.newQuad(x, y, 8, 8, w, h)
		x = x + 8
		counter = counter + 1
	end

	return quads
end

-- 1 blue, 2 green, 3 red, 4 violet, 5 white, 6 black, 7 gold
function GenerateQuadsBalls (atlas)

	return GenerateQuadsX (atlas, 0, 192, 8, 8, 7, 1)
--	local b2 = GenerateQuadsX (atlas, 128, 56, 8, 8, 4)
end

function GenerateQuadsBricks (atlas)
	return table.slice(GenerateQuads(atlas, 32, 16), 1, 22)
end

--[[
 1 grow, 2, shrink, 3 lifeUp, 4 lifeDown
 5 red ball small, 6 green ball big, 7 2 balls, 8 key
 9 red, 10 green, 11 blue, 12 white
]]
function GenerateQuadsPowerups (atlas)
	local p1 = GenerateQuadsX(atlas, 128,  48, 16, 16, 4)
	local p2 = GenerateQuadsX(atlas, 128,  80, 16, 16, 4)
	local p3 = GenerateQuadsX(atlas, 128, 112, 16, 16, 4)
	
	return arrayMerge(p1, p2, p3)
end

function GenerateQuadsHearts (atlas)	
	return GenerateQuadsX (atlas, 64, 192, 10, 9, 2)
end

function GenerateQuadsArrows (atlas)
	return GenerateQuadsX (atlas, 128, 176, 24, 24, 2)
end


