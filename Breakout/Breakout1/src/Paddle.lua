Paddle = Class{}	-- player


Paddle.height = 16

function Paddle:init(skin)

	self.width = 0
	
	-- four paddle sizes use;
	-- 2 starting size, smallest is too tough to start with
	self.size = 2
	self:setSize(2)

	-- middle
	self.x = VIRTUAL_WIDTH / 2 - self.width/2
	self.y = VIRTUAL_HEIGHT - self.height - 1

	-- no velocity
	self.dx = 0
	self.keys = 0
	
	-- color
	self.skin = skin

	
	self.score = 0	-- each player own score
	self.life = 3	-- reset life on new player
end



function Paddle:centerBalls ()
	for i = 1, #O.balls do
		local ball = O.balls[i]
		ball.x = O.paddle.x + (O.paddle.width / 2) - ball.width/2
		ball.y = O.paddle.y - ball.height
		ball:rndUp()
	end
end


function Paddle:lifeUp ()
	if O.sfx then gSounds['recover']:play() end
	if self.life < MAX_LIFE then
		self.life = self.life + 1
	end
	return self.life
end

function Paddle:lifeDown ()
	if O.sfx then gSounds['hurt']:play() end
	if self.life > 0 then
		self.life = self.life - 1
	end
	return self.life
end


function Paddle:grow ()
	if O.sfx then gSounds['recover']:play() end
	self:setSize(self.size + 1)
end

function Paddle:shrink ()
	if O.sfx then gSounds['hurt']:play() end
	self:setSize(self.size - 1)
end

function Paddle:setSize (size)
	if size < MIN_PADDLE_SIZE or size > MAX_PADDLE_SIZE then return end
	
	if size > self.size then
		self.x = self.x - 16
	elseif size < self.size then
		self.x = self.x + 16
	end
	
	self.size = size
	self.width = 32 * self.size
end


function Paddle:update(dt)

	if keyDown['left'] then
		self.dx = -PADDLE_SPEED
	elseif keyDown['right'] then
		self.dx = PADDLE_SPEED
	else
		self.dx = 0
	end

	if self.dx < 0 then
		self.x = math.max(0, self.x + self.dx * dt)
	elseif self.dx > 0 then
		self.x = math.min(VIRTUAL_WIDTH - self.width, self.x + self.dx * dt)
	end
end

function Paddle:render()
	love.graphics.draw(gTextures['main'], gFrames['paddles'][self.size + 4 * (self.skin - 1)],
		math.floor(self.x), math.floor(self.y))
end