StartState = Class{__includes = BaseState}

function StartState:init ()
self.help = [[
space - pause
m - music
n - sfx
f - fps
enter - serve
esc - back/quit
h - textures
]]
end

function StartState:enter (p)
--	print('StartState')

	O.bricks =	LevelMaker.createMap(1)
	O.level =	1
end


-- Start or High Scores
local highlighted = 1

function StartState:update(dt)
	if keyDown['up'] or keyDown['down'] then
		keyDown['up'] = false
		keyDown['down'] = false
		
		highlighted = highlighted == 1 and 2 or 1
		if O.sfx then gSounds['paddle_hit']:play() end
	end

	if keyDown['h'] then
	keyDown['h'] = false
		gStateMachine:change('help')
	end
	
	-- select option
	if keyDown['enter'] or keyDown['return'] then
		keyDown['enter'] = false
		keyDown['return'] = false
		
		if O.sfx then gSounds['confirm']:play() end
		
		if highlighted == 1 then 
			gStateMachine:change('paddleSelect')
		else
			gStateMachine:change('hiScore')
		end
	end
	
	
	
	if keyDown['escape'] then love.event.quit() end
end

function StartState:render()

	love.graphics.setColor(1, 1, 1, 1)
	love.graphics.setFont(gFonts['large'])
	love.graphics.printf("BREAKOUT", 0, VIRTUAL_HEIGHT / 3, VIRTUAL_WIDTH, 'center')

	
	love.graphics.setFont(gFonts['small'])
	love.graphics.printf(self.help, 10, VIRTUAL_HEIGHT/2, VIRTUAL_WIDTH, 'left')
	
	-- instructions
	love.graphics.setFont(gFonts['medium'])
	if highlighted == 1 then
		love.graphics.setColor(0.5, 0.5, 1, 1)
	end
	love.graphics.printf("START", 0, VIRTUAL_HEIGHT / 2 + 70, VIRTUAL_WIDTH, 'center')

	
	love.graphics.setColor(1, 1, 1, 1)
	if highlighted == 2 then
		love.graphics.setColor(0.5, 0.5, 1, 1)
	end
	love.graphics.printf("HIGH- SCORES", 0, VIRTUAL_HEIGHT / 2 + 90, VIRTUAL_WIDTH, 'center')

	
	
	-- reset color
	love.graphics.setColor(1, 1, 1, 1)
end