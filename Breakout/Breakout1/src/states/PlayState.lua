PlayState = Class{__includes = BaseState}

function PlayState:enter(p)
--	print('PlayState')
	self.bricksNum = self:countActiveBricks()
--	print(self.bricksNum)
	self.pause = false
end

function PlayState:update(dt)
	if keyDown['escape'] then
		keyDown['escape'] = false
		gStateMachine:change('start') 
--		love.event.quit()
	end
	if self.pause then
		if keyDown['space'] then
			keyDown['space'] = false
			self.pause = false
			if O.sfx then gSounds['pause']:play() end
		else
			return
		end
	elseif keyDown['space'] then
		keyDown['space'] = false
		self.pause = true
		if O.sfx then gSounds['pause']:play() end
		return
	end

--	for i = 1, #O.balls do
--		O.balls[i]:update(dt)
--	end
	O.paddle:update(dt)

	
	for k, ball in pairs(O.balls) do
		ball:update(dt)
		if not ball.inPlay then
			table.remove(O.balls, k)
			goto continue
		end
		
		if collision(ball, O.paddle) then
			if O.sfx then gSounds['paddle_hit']:play() end
			ball:bounceY(O.paddle.y - ball.height)
	
			--[[ this is shit!!! use paddle.dx directly
			-- if we hit the paddle on its left side while moving left...
			if O.paddle.dx < 0 and O.ball.x + O.ball.width < O.paddle.x + (O.paddle.width / 2) then
				O.ball.dx = -50 + -(8 * (O.paddle.x + O.paddle.width / 2 - (O.ball.x + O.ball.width)))
			
				-- else if we hit the paddle on its right side while moving right...
			elseif O.paddle.dx > 0 and O.ball.x > O.paddle.x + (O.paddle.width / 2) then
				O.ball.dx = 50 + (8 * math.abs(O.paddle.x + O.paddle.width / 2 - O.ball.x))
			end
			]]
			-- paddle moving
			if O.paddle.dx ~= 0 then
				ball.dx = ball.dx + math.floor(O.paddle.dx / 5)
			end
		end
		::continue::
	end

	if #O.balls == 0 then
		if O.paddle:lifeDown() < 1 then
			gStateMachine:change('gameOver')
		else
			Ball:add()		-- new ball, you just lost one
			-- O.paddle = Paddle()			-- old paddle, reset paddle?
			O.paddle:setSize(2)
			gStateMachine:change('serve')
		end
	end
	
	
	
	for i = 1, #O.bricks do
		local brick = O.bricks[i]
	
		for k, ball in pairs(O.balls) do
		if brick.inPlay and collision(ball, brick) then
			brick:hit()
			if not brick.inPlay then self.bricksNum = self.bricksNum - 1 end
			--print(self.bricksNum)

			-- if brick.color == 5 and brick.tier == 1 then nothing -- brown unlocked
			O.paddle.score = O.paddle.score + (brick.tier * 200 + brick.color * 25)
			
			if self.bricksNum <= 0 then
				if O.sfx then gSounds['victory']:play() end
				gStateMachine:change('victory') -- level over
				return
			end
			
			ball:hitBrick(brick)

			break -- not to hit 2 bricks in corner!
		end
		end
		
		
		brick:update(dt)
		if brick.powerup and brick.powerup.falling and collision(O.paddle, brick.powerup) then
			brick.powerup:consumed()
		end
	end
	
end

function PlayState:render()

	for i = 1, #O.balls do
		O.balls[i]:render()	-- must be first to render
	end
	O.paddle:render()
	for i = 1, #O.bricks do
		O.bricks[i]:render()
	end
	
	renderScore(O.paddle.score)
	renderLife(O.paddle.life)
	
	-- pause text, if paused
	if self.pause then
		love.graphics.setFont(gFonts['large'])
		love.graphics.printf("PAUSED", 0, VIRTUAL_HEIGHT / 2 - 16, VIRTUAL_WIDTH, 'center')
	end
end

-- ugly
function PlayState:checkVictory ()
	
	for k, brick in pairs(O.bricks) do
		if brick.inPlay then return false end
	end

	return true
end


function PlayState:countActiveBricks ()

	local num = 0
	
	for k, brick in pairs(O.bricks) do
		if brick.inPlay then num = num + 1 end
	end
	
	return num
end