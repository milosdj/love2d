ServeState = Class{__includes = BaseState}

function ServeState:enter(p)	
--	print('ServeState')
end

function ServeState:update(dt)
	-- have the ball track the player
	O.paddle:update(dt)
	O.paddle:centerBalls()

	if keyDown['enter']  or keyDown['return'] then
		keyDown['enter'] = false		
		keyDown['return'] = false		
		gStateMachine:change('play')
	end

	if keyDown['escape'] then
		keyDown['escape'] = false
		--love.event.quit()
		gStateMachine:change('start')
	end
end

function ServeState:render()

	for i = 1, #O.balls do
		O.balls[i]:render()
	end
	O.paddle:render()
	for i = 1, #O.bricks do
		O.bricks[i]:render()
	end

	renderScore(O.paddle.score)
	renderLife(O.paddle.life)

	love.graphics.setFont(gFonts['medium'])
	love.graphics.printf('Press Enter to play!', 0, VIRTUAL_HEIGHT / 2, VIRTUAL_WIDTH, 'center')
end