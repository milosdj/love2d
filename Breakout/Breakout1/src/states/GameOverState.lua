GameOverState = Class{__includes = BaseState}

function GameOverState:enter(p)
--	print('GameOverState')
--	printT(O.hiScores)
--	print(O.paddle.score)
end

function GameOverState:update(dt)
	if keyDown['enter'] or keyDown['return'] then
		keyDown['enter'] = false
		keyDown['return'] = false
		
		local enterHS = false
		
		for i = 1, 10 do		
			local set = O.hiScores[i] or {}
			local scr = tonumber(set[2]) or 0
			if O.paddle.score > scr then
				enterHS = true
				break
			end
		end
		
		if enterHS then
			gStateMachine:change('enterHiScore')
		else
			gStateMachine:change('start')
		end
		
	end

	if keyDown['escape'] then
		keyDown['escape'] = false
		--love.event.quit()
		gStateMachine:change('start')
	end
end

function GameOverState:render()
	love.graphics.setFont(gFonts['large'])
	love.graphics.printf('GAME OVER', 0, VIRTUAL_HEIGHT / 3, VIRTUAL_WIDTH, 'center')

	love.graphics.setFont(gFonts['medium'])
	love.graphics.printf('Final Score: '..tostring(O.paddle.score), 0, VIRTUAL_HEIGHT / 2, VIRTUAL_WIDTH, 'center')
	love.graphics.printf('Press Enter to play again!', 0, VIRTUAL_HEIGHT - VIRTUAL_HEIGHT / 4, VIRTUAL_WIDTH, 'center')
end