HiScoreState = Class{__includes = BaseState}



function HiScoreState:enter(p)
--	print('HiScoreState')
--	printT(O.hiScores)
end

function HiScoreState:update(dt)
	-- return to the start screen if we press escape
	if keyDown['escape'] then
		keyDown['escape'] = false
		
		if O.sfx then gSounds['wall_hit']:play() end
		
		gStateMachine:change('start')
	end
end

function HiScoreState:render()
	love.graphics.setFont(gFonts['large'])
	love.graphics.printf('High Scores', 0, 20, VIRTUAL_WIDTH, 'center')

	love.graphics.setFont(gFonts['medium'])

	-- iterate over all high score indices in our high scores table
	for i = 1, 10 do
		local l = O.hiScores[i] or {'---', '---'}
		local name = l[1] or '---'
		local score = l[2] or '---'
		
		love.graphics.printf(tostring(i)..'.', VIRTUAL_WIDTH / 4, 60 + i * 13, 50, 'left')
		love.graphics.printf(tostring(name), VIRTUAL_WIDTH / 4 + 38, 60 + i * 13, 50, 'right')		
		love.graphics.printf(tostring(score), VIRTUAL_WIDTH / 2, 60 + i * 13, 100, 'right')
	end

	love.graphics.setFont(gFonts['small'])
	love.graphics.printf("Press Escape to return to the main menu!", 0, VIRTUAL_HEIGHT - 18, VIRTUAL_WIDTH, 'center')
end


-- return full or empty {}
function HiScoreState:loadHiScores ()
	
	if not love.filesystem.getInfo(gSaveFileName) then
		love.filesystem.write(gSaveFileName, '') 
	end
	
	love.filesystem.setIdentity('breakout')
	local res = csv:fromFile(gSaveFileName)
	return res ~= nil and res or {}
end





function HiScoreState:loadHiScore_old()
	love.filesystem.setIdentity('breakout')

	local fileName = 'breakout.lst'
	
	-- if the file doesn't exist, initialize it with some default scores
	
--	if not love.filesystem.exists(fileName) then
	if not love.filesystem.getInfo(fileName) then
		local scores = ''
		for i = 10, 1, -1 do
			scores = scores .. 'CTO\n'
			scores = scores .. tostring(i * 1000) .. '\n'
		end

		love.filesystem.write(fileName, scores)
	end

	-- flag for whether we're reading a name or not
	local name = true
	local currentName = nil
	local counter = 1

	-- initialize scores table with at least 10 blank entries
	local scores = {}

	for i = 1, 10 do
		-- blank table; each will hold a name and a score
		scores[i] = {
			name = nil,
			score = nil
		}
	end

	-- iterate over each line in the file, filling in names and scores
	for line in love.filesystem.lines(fileName) do
		if name then
			scores[counter].name = string.sub(line, 1, 3)
		else
			scores[counter].score = tonumber(line)
			counter = counter + 1
		end

		-- flip the name flag
		name = not name
	end

	return scores
end
