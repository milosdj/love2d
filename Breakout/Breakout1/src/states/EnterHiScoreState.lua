EnterHiScoreState = Class{__includes = BaseState}

-- individual chars of our string
local chars = {
	[1] = 65,
	[2] = 65,
	[3] = 65,
}

-- char we're currently changing
local hilightedChar = 1

function EnterHiScoreState:enter(p)
--	print('EnterHiScoreState')
--	printT(O.hiScores)

	-- reset keys
	hilightedChar = 1
	chars = {
	[1] = 65,
	[2] = 65,
	[3] = 65,
	}
end

function EnterHiScoreState:update(dt)
	if keyDown['enter'] or keyDown['return'] then
		keyDown['enter'] = false
		keyDown['return'] = false
	

		local name = string.char(chars[1]) .. string.char(chars[2]) .. string.char(chars[3])
		self:saveScore(name, O.paddle.score)

		gStateMachine:change('hiScore')
	end

	self:getKeys()
	
	if keyDown['escape'] then
		keyDown['escape'] = false
		--love.event.quit()
		gStateMachine:change('start')
	end
end

function EnterHiScoreState:render()
	love.graphics.setFont(gFonts['medium'])
	love.graphics.printf('Your score: '..O.paddle.score, 0, 30,	VIRTUAL_WIDTH, 'center')

	love.graphics.setFont(gFonts['large'])
	
	
	-- render all three characters of the name
	if hilightedChar == 1 then
		love.graphics.setColor(103/255, 1,1,1)
	end
	love.graphics.print(string.char(chars[1]), VIRTUAL_WIDTH / 2 - 28, VIRTUAL_HEIGHT / 2)
	love.graphics.setColor(1,1,1,1)

	if hilightedChar == 2 then
		love.graphics.setColor(103/255, 1,1,1)
	end
	love.graphics.print(string.char(chars[2]), VIRTUAL_WIDTH / 2 - 6, VIRTUAL_HEIGHT / 2)
	love.graphics.setColor(255, 255, 255, 255)

	if hilightedChar == 3 then
		love.graphics.setColor(103/255, 1,1,1)
	end
	love.graphics.print(string.char(chars[3]), VIRTUAL_WIDTH / 2 + 20, VIRTUAL_HEIGHT / 2)
	love.graphics.setColor(1,1,1,1)
	
	love.graphics.setFont(gFonts['small'])
	love.graphics.printf('Press Enter to confirm!', 0, VIRTUAL_HEIGHT - 18, VIRTUAL_WIDTH, 'center')
end




function EnterHiScoreState:getKeys ()

	-- scroll through character slots
	if keyDown['left'] and hilightedChar > 1 then
		keyDown['left'] = false
		hilightedChar = hilightedChar - 1
		if O.sfx then gSounds['select']:play()end
	elseif keyDown['right'] and hilightedChar < 3 then
		keyDown['right'] = false
		hilightedChar = hilightedChar + 1
		if O.sfx then gSounds['select']:play() end
	end

	-- scroll through characters
	if keyDown['up'] then
		keyDown['up'] = false
		chars[hilightedChar] = chars[hilightedChar] + 1
		if chars[hilightedChar] > 90 then
			chars[hilightedChar] = 65
		end
	elseif keyDown['down'] then
		keyDown['down'] = false
		chars[hilightedChar] = chars[hilightedChar] - 1
		if chars[hilightedChar] < 65 then
			chars[hilightedChar] = 90
		end
	end
end



function EnterHiScoreState:saveScore (name, score)

	score = tonumber(score)
	local index = #O.hiScores + 1		-- last place
	
	for i = 1, 10 do
		-- 1 name, 2 score
		local row = O.hiScores[i] or {}
		local scr = tonumber(row[2]) or 0
		if score > scr then
			index = i
			break
		end
	end
	
	table.insert(O.hiScores, index, {name, score})
	
	local len = #O.hiScores - 10
	for i = 1, len do
		table.remove(O.hiScores)	-- remove last item
	end
	
--	local scoreStr = csv:table2str(self.hiScores)
--	print (scoreStr)
	love.filesystem.write(gSaveFileName, csv:table2str(O.hiScores))
end



