PaddleSelectState = Class{__includes = BaseState}

function PaddleSelectState:enter(p)
--	print('PaddleSelect')
end

function PaddleSelectState:init()
	-- the paddle we're highlighting
	self.currentPaddle = 1
	
	self.paddlesLen = #gFrames['paddles'] / 4	-- 4 sizes
end

function PaddleSelectState:update(dt)
	if keyDown['left'] then
		keyDown['left'] = false
		if self.currentPaddle == 1 then
			if O.sfx then 
				gSounds['no_select']:stop()
				gSounds['no_select']:play()
			end
		else
			if O.sfx then gSounds['select']:play() end
			self.currentPaddle = self.currentPaddle - 1
		end
	elseif keyDown['right'] then
		keyDown['right'] = false
		if self.currentPaddle == self.paddlesLen then
			if O.sfx then 
				gSounds['no_select']:stop()
				gSounds['no_select']:play()
			end
		else
			if O.sfx then gSounds['select']:play() end
			self.currentPaddle = self.currentPaddle + 1
		end
	end

	-- select paddle and move on to the serve state, passing in the selection
	if keyDown['return'] or keyDown['enter'] then
		keyDown['enter'] = false
		keyDown['return'] = false
		if O.sfx then gSounds['confirm']:play() end

		O.paddle =	Paddle(self.currentPaddle)
		O.balls = {}
		Ball:add()	-- cant add ball without paddle
		gStateMachine:change('serve')
	end

	if keyDown['escape'] then
		keyDown['escape'] = false
	--love.event.quit()
		gStateMachine:change('start')
	end
end

function PaddleSelectState:render()
	-- instructions
	love.graphics.setFont(gFonts['medium'])
	love.graphics.printf("Select your paddle with left and right!", 0, VIRTUAL_HEIGHT / 4, VIRTUAL_WIDTH, 'center')
	love.graphics.setFont(gFonts['small'])
	love.graphics.printf("(Press Enter to continue!)", 0, VIRTUAL_HEIGHT / 3, VIRTUAL_WIDTH, 'center')
	
	-- left arrow; should render normally if we're higher than 1, else
	-- in a shadowy form to let us know we're as far left as we can go
	if self.currentPaddle == 1 then
		-- tint; give it a dark gray with half opacity
		love.graphics.setColor(40/255, 40/255, 40/255, 0.5)
	end
	
	love.graphics.draw(gTextures['main'], gFrames['arrows'][1], VIRTUAL_WIDTH / 4 - 24, VIRTUAL_HEIGHT - VIRTUAL_HEIGHT / 3)
   
	-- reset drawing color to full white for proper rendering
	love.graphics.setColor(1,1,1,1)

	-- right arrow; should render normally if we're less than 4, else
	-- in a shadowy form to let us know we're as far right as we can go
	if self.currentPaddle == 4 then
		-- tint; give it a dark gray with half opacity
		love.graphics.setColor(40/255, 40/255, 40/255, 0.5)
	end
	
	love.graphics.draw(gTextures['main'], gFrames['arrows'][2], VIRTUAL_WIDTH - VIRTUAL_WIDTH / 4,
		VIRTUAL_HEIGHT - VIRTUAL_HEIGHT / 3)
	
	-- reset drawing color to full white for proper rendering
	love.graphics.setColor(1,1,1,1)

	-- draw the paddle itself, based on which we have selected
	love.graphics.draw(gTextures['main'], gFrames['paddles'][2 + 4 * (self.currentPaddle - 1)],
		VIRTUAL_WIDTH / 2 - 32, VIRTUAL_HEIGHT - VIRTUAL_HEIGHT / 3)
end