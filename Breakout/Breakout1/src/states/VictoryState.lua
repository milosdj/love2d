VictoryState = Class{__includes = BaseState}

-- level over
function VictoryState:enter(p)
--	print('VicrotyState')
end

function VictoryState:update(dt)

	O.paddle:update(dt)
	O.paddle:centerBalls()
	
	-- go to play screen if the player presses Enter
	if keyDown['enter'] or keyDown['return'] then
		keyDown['enter'] = false
		keyDown['return'] = false
		
		O.level = O.level + 1
		O.bricks = LevelMaker.createMap(O.level),	-- need new bricks
		-- O.ball = Ball()			-- use winning ball
		-- O.paddle:lifeUp()
		-- O.paddle.score = O.paddle.score + 10000
		gStateMachine:change('serve')
	end
end

function VictoryState:render()

	for i = 1, #O.balls do
		O.balls[i]:render()
	end
	
	O.paddle:render()

	renderLife(O.paddle.life)
	renderScore(O.paddle.score)

	-- level complete text
	love.graphics.setFont(gFonts['large'])
	love.graphics.printf("Level "..tostring(O.level).." complete!", 0, VIRTUAL_HEIGHT / 4, VIRTUAL_WIDTH, 'center')

	-- instructions text
	love.graphics.setFont(gFonts['medium'])
	love.graphics.printf('Press Enter to continue!', 0, VIRTUAL_HEIGHT / 2, VIRTUAL_WIDTH, 'center')
end