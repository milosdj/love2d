HelpState = Class{__includes = BaseState}

function HelpState:init ()

	self.index = 0

	self.cx = 0
	self.cy = 0
	self.cw = VIRTUAL_WIDTH
	self.ch = VIRTUAL_HEIGHT
	
end

function HelpState:update (dt)

	if keyDown['left'] then
		if self.cx > -self.cw then self.cx = self.cx - 1 end	
	end
	if keyDown['right'] then
		if self.cx < self.cw then self.cx = self.cx + 1 end
	end
	if keyDown['up'] then
		if self.cy > -self.ch then self.cy = self.cy - 1 end	
	end
	if keyDown['down'] then
		if self.cy < self.ch then self.cy = self.cy + 1 end
	end
	
	if keyDown['escape'] then
		keyDown['escape'] = false
		gStateMachine:change('start')
	end
end

function HelpState:render ()
	love.graphics.setFont(gFonts['small'])
	self:drawFrames(self.cx, self.cy)
end

function HelpState:drawFrames (cx, cy)

	x = cx or 0
	y = cy or 0
	local qx, qy, qw, qh
	local margin = 1
	local maxH = 0

	
	for fName, fTable in pairs(gFrames) do
		love.graphics.printf(fName, x, y, VIRTUAL_WIDTH, 'left')
		y = y + 10 + margin
		
		for i, quad in ipairs(fTable) do

			qx, qy, qw, qh = quad:getViewport()
			if qh > maxH then maxH = qh end
			
			if x + qw + margin >= self.cw then			
				y = y + maxH + margin
				maxH = qh
				x = cx
			end
			
			love.graphics.draw(gTextures['main'], quad, math.floor(x), math.floor(y))
			x = x + qw + margin
		end
		y = y + maxH + margin
		maxH = 0
		x = cx
	end
end