LevelMaker = Class{} 

-- creates table of Bricks
function LevelMaker.createMap1(level)
	local bricks = {}
	local offsetY = 30
	
	-- number of rows
	local numRows = math.random(1, 5)

	-- number of columns
	local numCols = math.random(7, 13)

	-- lay out bricks such that they touch each other and fill the space
	for y = 1, numRows do
		for x = 1, numCols do
			b = Brick(
				-- x-coordinate
				(x-1)					-- decrement x by 1 because tables are 1-indexed, coords are 0
				* 32					-- multiply by 32, the brick width
				+ 8						-- screen should have 8 pixels of padding; we can fit 13 cols + 16 pixels total
				+ (13 - numCols) * 16,	-- left-side padding for when there are fewer than 13 columns
				
				-- y-coordinate
				y * 16 + offsetY	  -- just use y * 16, since we need top padding anyway
			) 

			table.insert(bricks, b)
		end
	end

	return bricks
end




-- global patterns (used to make the entire map a certain shape)
-- NONE = 1
-- SINGLE_PYRAMID = 2
-- MULTI_PYRAMID = 3
-- 
-- -- per-row patterns
-- SOLID = 1		   -- all colors the same in this row
-- ALTERNATE = 2	   -- alternate colors
-- SKIP = 3			-- skip every other block
-- NONE = 4			-- no blocks this row


function LevelMaker.createMap(level)
	local bricks = {}
	local offsetY = 10

	
	-- randomly choose the number of rows
	local numRows = math.random(math.min(5, level) , math.min(6, level+1))

	-- randomly choose the number of columns, ensuring odd
	local numCols = math.random(7, 13)
	numCols = numCols % 2 == 0 and (numCols + 1) or numCols
	

	local lockedBrick = {x=0, y=0}
	local keyBrick = {x=0, y=0}
	
	if math.random() < 0.25 then
--	if true then
		lockedBrick.x = math.random(numCols)
		lockedBrick.y = math.random(numRows)

		keyBrick.x = math.random(numCols)
		keyBrick.y = math.random(numRows)		
	end
	
	if lockedBrick.x == keyBrick.x and lockedBrick.y == keyBrick.y then
		if lockedBrick.x == 1 then
			keyBrick.x = keyBrick.x + 1
		else
			keyBrick.x = keyBrick.x - 1
		end
	end
	
	--	0-3	MAX_TIER
	local highestTier = math.min(MAX_TIER, math.floor(level / 5))

	-- 0-4 MAX_COLOR
	local highestColor = math.min(MAX_COLOR, level % 5 + 1)

	-- lay out bricks such that they touch each other and fill the space
	for y = 1, numRows do
		-- whether we want to enable skipping for this row
		local skipPattern = math.random(1, 2) == 1 and true or false

		-- whether we want to enable alternating colors for this row
		local alternatePattern = math.random(1, 2) == 1 and true or false
		
		-- choose two colors to alternate between
		local alternateColor1 = math.random(0, highestColor)
		local alternateColor2 = math.random(0, highestColor)
		local alternateTier1 = math.random(0, highestTier)
		local alternateTier2 = math.random(0, highestTier)
		
		-- used only when we want to skip a block, for skip pattern
		local skipFlag = math.random(2) == 1 and true or false

		-- used only when we want to alternate a block, for alternate pattern
		local alternateFlag = math.random(2) == 1 and true or false

		-- solid color we'll use if we're not alternating
		local solidColor = math.random(1, highestColor)
		local solidTier = math.random(0, highestTier)

		for x = 1, numCols do
			-- if skipping is turned on and we're on a skip iteration...
			if skipPattern and skipFlag then
				-- turn skipping off for the next iteration
				skipFlag = not skipFlag

				-- Lua doesn't have a continue statement, so this is the workaround
				goto continue
			else
				-- flip the flag to true on an iteration we don't use it
				skipFlag = not skipFlag
			end

			b = Brick(
				-- x-coordinate
				(x-1)					-- decrement x by 1 because tables are 1-indexed, coords are 0
				* 32					-- multiply by 32, the brick width
				+ 8						-- the screen should have 8 pixels of padding; we can fit 13 cols + 16 pixels total
				+ (13 - numCols) * 16,	-- left-side padding for when there are fewer than 13 columns
				
				-- y-coordinate
				y * 16 + offsetY	  -- just use y * 16, since we need top padding anyway
			)
			
			-- if we're alternating, figure out which color/tier we're on
			if alternatePattern and alternateFlag then
				b.color = alternateColor1
				b.tier = alternateTier1
				alternateFlag = not alternateFlag
			else
				b.color = alternateColor2
				b.tier = alternateTier2
				alternateFlag = not alternateFlag
			end

			-- if not alternating and we made it here, use the solid color/tier
			if not alternatePattern then
				b.color = solidColor
				b.tier = solidTier
			end 

			if math.random() < 0.35 then
				b.powerup = PowerUp(b.x, b.y, math.random(#gFrames['powerups']))
			end
			
			
			
			table.insert(bricks, b)

			-- Lua's version of the 'continue' statement
			::continue::
			-- chance that only key will be in locked brick
			if lockedBrick.x == x and lockedBrick.y == y then
				b.color = LOCKED_COLOR	-- brown
				b.tier = 1	-- locked
			end
			if keyBrick.x == x and keyBrick.y == y then
				b.powerup = PowerUp(b.x, b.y, 8)	-- at least one key
			end
			
		end
	end 

	-- in the event we didn't generate any bricks, try again
	if #bricks == 0 then
		return self.createMap(level)
	else
		return bricks
	end
end