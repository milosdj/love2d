PowerUp = Class{}



PowerUp.width = 16
PowerUp.height = 16

function PowerUp:init(x, y, id)

	self.dy = POWER_UP_SPEED
	self.x = x + Brick.width/2 - self.width/2
	self.y = y + Brick.height/2 - self.width/2
	self.id = id
	
	self.inPlay = true
	self.falling = false
end


function PowerUp:consumed ()

	self:die()
--[[
 1 grow, 2, shrink, 3 lifeUp, 4 lifeDown
 5 red ball small, 6 green ball big, 7 2 balls, 8 key
 9 red, 10 green, 11 blue, 12 white
]]

	if self.id == 1 then		-- _+
	
		O.paddle.score = O.paddle.score + 1000
		O.paddle:grow()	
		if O.sfx then gSounds['score']:play() end

	elseif self.id == 2 then		-- _-
	
		O.paddle.score = O.paddle.score + 5000
		O.paddle:shrink()
	
	elseif self.id == 3 then			-- lifeUp

		O.paddle.score = O.paddle.score + 5000
		O.paddle:lifeUp()

	elseif self.id == 4 then		-- lifeDown

		O.paddle.score = O.paddle.score + 10000
		O.paddle:lifeDown()
	
	elseif self.id == 5 then			-- +1 rnd small ball

		Ball:add(nil, 1)
		O.paddle.score = O.paddle.score + 1000

	elseif self.id == 6 then			-- + 1 rnd big ball

		Ball:add(nil, 3)
		O.paddle.score = O.paddle.score + 1000

	elseif self.id == 7 then			-- + 2 rnd balls

		Ball:add(nil, nil)
		Ball:add(nil, nil)
		O.paddle.score = O.paddle.score + 1000

	elseif self.id == 8 then		-- key
	
		O.paddle.score = O.paddle.score + 2000
		O.paddle.keys = O.paddle.keys + 1
		if O.sfx then gSounds['confirm']:play() end

	elseif self.id == 9 then		-- red X all red and small
--  1 blue, 2 green, 3 red, 4 violet, 5 white, 6 black, 7 gold

		O.paddle.score = O.paddle.score + 100
		Ball:setSizeBalls(3, 1)

	elseif self.id == 10 then		-- greenX all green and speed down 

		O.paddle.score = O.paddle.score + 200
		Ball:setSpeedBalls(2, 0.5)
		
	elseif self.id == 11 then		-- blueX all blue and big

		O.paddle.score = O.paddle.score + 1000		
		Ball:setSizeBalls(1, 3)

	elseif self.id == 12 then		-- whiteX all white and speed up

		O.paddle.score = O.paddle.score + 2000
		Ball:setSpeedBalls(5, 1.25)
	end
end


function PowerUp:die ()
	self.inPlay = false
	self.falling = false
end

function PowerUp:update (dt)
--	self.x = self.x + self.dx * dt
	self.y = self.y + self.dy * dt

	-- pao dole
	if self.y > VIRTUAL_HEIGHT then
		self:die()
	end
end

function PowerUp:render ()
	if self.inPlay then
		love.graphics.draw(gTextures['main'], gFrames['powerups'][self.id],
		math.floor(self.x), math.floor(self.y))
	end
end