Ball = Class{}


function Ball:init(skin, size)

	if size < 1 or size > 3 then size = 2 end

	-- vector, move
	self.dy = 0
	self.dx = 0

	self.x = 0
	self.y = 0
	-- ball texture
	self.skin = skin
	self.size = size
	self.width = 2^(size+1)
	self.height = self.width	
	
	self.inPlay = true
end

-- cant add ball without paddle
function Ball:add (skin, size)
	skin = skin or math.random(#gFrames['balls'])
	size = size or math.random(1,3)
	
	local ball = Ball(skin, size)
	ball:rndUp()
	ball:onPaddle()
	O.balls[#O.balls+1] = ball
end

function Ball:setSpeedBalls (skin, speed)
	for i=1, #O.balls do
		O.balls[i].skin = skin
		O.balls[i].dx = O.balls[i].dx * speed
		O.balls[i].dy = O.balls[i].dy * speed
	end
end

function Ball:setSizeBalls (skin, size)
	for i=1, #O.balls do
		O.balls[i].skin = skin
		O.balls[i]:setSize(size)
	end
end

-- ball in middle of screen, with no movement
function Ball:reset()
	self.x = VIRTUAL_WIDTH / 2 - self.width/2
	self.y = VIRTUAL_HEIGHT / 2 - self.height/2
	self.dx = 0
	self.dy = 0
end

function Ball:rndUp ()
	self.dx = math.random(-50, 50)
	self.dy = math.random(-150, -100)
end


function Ball:onPaddle ()
	self.x = O.paddle.x + (O.paddle.width / 2) - self.width/2
	self.y = O.paddle.y - self.height
end

--	1-3
function Ball:setSize (size)
	if size < 1 or size > 3 then size = 2 end

	local new_w = 2^(size+1)
	local offset = math.abs(new_w - self.width) / 2

	if size > self.size then
		self.x = self.x - offset
		self.y = self.y - offset
	elseif size < self.size then
		self.x = self.x + offset
		self.y = self.y + offset
	end

	self.size = size
	self.width = new_w
	self.height = new_w
end


function Ball:update(dt)

	if not self.inPlay then return end
	
	self.x = self.x + self.dx * dt
	self.y = self.y + self.dy * dt

	-- ball hitWall()
	-- top wall
	if self.y <= 0 then
		self:bounceY(0)
		if O.sfx then gSounds['wall_hit']:play() end
	end	
	
	-- bottom wall
	if self.y >= VIRTUAL_HEIGHT then
		self.inPlay = false
		if O.sfx then gSounds['hurt']:play() end
	end	
	
	-- left wall
	if self.x <= 0 then
		self:bounceX(0)
		if O.sfx then gSounds['wall_hit']:play() end
	end
	-- right wall
	if self.x + self.width >= VIRTUAL_WIDTH then
		self:bounceX(VIRTUAL_WIDTH - self.width)
		if O.sfx then gSounds['wall_hit']:play() end
	end
end

function Ball:bounceX (x, p)
	p = p or 10
	self.x = x
	self.dx = -self.dx + math.random(-p, p)
	self.dy = self.dy + math.random(-p, p)
end

function Ball:bounceY (y, p)
	p = p or 10
	self.y = y
	self.dx = self.dx + math.random(-p, p)
	self.dy = -self.dy + math.random(-p, p)
end

-- original
function Ball:hitBrick (brick)

	-- left edge
	if self.x + 2 < brick.x and self.dx > 0 then
		-- flip x velocity and reset position outside of brick
		self:bounceX(brick.x - self.width)
	-- right edge; only check if we're moving left
	elseif self.x + self.width -2 > brick.x + brick.width and self.dx < 0 then
		-- flip x velocity and reset position outside of brick
		self:bounceX(brick.x + brick.width)
	
	-- top edge if no X collisions, always check
	elseif self.y < brick.y then
		-- flip y velocity and reset position outside of brick
		self:bounceY(brick.y - self.height)
	
	-- bottom edge if no X collisions or top collision, last possibility
	else
		-- flip y velocity and reset position outside of brick
		self:bounceY(brick.y + brick.height)
	end

	-- speed up game
	self.dy = self.dy * SPEED_UP

end
--[[
-- collision code for bricks
--
-- we check to see if the opposite side of our velocity is outside of the brick;
-- if it is, we trigger a collision on that side. else we're within the X + width of
-- the brick and should check to see if the top or bottom edge is outside of the brick,
-- colliding on the top or bottom accordingly 
--

-- left edge; only check if we're moving right, and offset the check by a couple of pixels
-- so that flush corner hits register as Y flips, not X flips
if self.ball.x + 2 < brick.x and self.ball.dx > 0 then
	
	-- flip x velocity and reset position outside of brick
	self.ball.dx = -self.ball.dx
	self.ball.x = brick.x - 8

-- right edge; only check if we're moving left, , and offset the check by a couple of pixels
-- so that flush corner hits register as Y flips, not X flips
elseif self.ball.x + 6 > brick.x + brick.width and self.ball.dx < 0 then
	
	-- flip x velocity and reset position outside of brick
	self.ball.dx = -self.ball.dx
	self.ball.x = brick.x + 32

-- top edge if no X collisions, always check
elseif self.ball.y < brick.y then
	
	-- flip y velocity and reset position outside of brick
	self.ball.dy = -self.ball.dy
	self.ball.y = brick.y - 8

-- bottom edge if no X collisions or top collision, last possibility
else
	
	-- flip y velocity and reset position outside of brick
	self.ball.dy = -self.ball.dy
	self.ball.y = brick.y + 16
end

-- slightly scale the y velocity to speed up the game, capping at +- 150
if math.abs(self.ball.dy) < 150 then
	self.ball.dy = self.ball.dy * 1.02
end

-- only allow colliding with one brick, for corners
break
]]




function Ball:render()
	if self.inPlay then
		local siz = self.size == 3 and 4 or self.size
		siz = siz / 2
		love.graphics.draw(gTextures['main'], gFrames['balls'][self.skin],
			math.floor(self.x), math.floor(self.y), 0, siz, siz)
	end
end