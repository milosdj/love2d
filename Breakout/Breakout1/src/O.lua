O = Class{}

-- ???
O.bricks = {}
O.balls = {}
O.paddle = nil
O.level = 1
O.hiScores = {}

O.sfx = true		-- n
O.music = false		-- m
O.fps = false		-- f

function O:init ()
	self:reset()
end

function O:reset ()
	self.bricks = {}
	self.balls = {}
	self.paddle = nil
	self.level = 1
	self.hiScores = {}
end
