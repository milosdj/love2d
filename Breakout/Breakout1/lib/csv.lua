--[[
19 10 2018

csv, moja radinost
]]


local csv = {}
setmetatable(csv, csv)

-- Used to escape "'s by toCSV
function csv:escapeQ (s)
	if string.find(s, '[,"]') then
		s = '"' .. string.gsub(s, '"', '""') .. '"'
	end
	return s
end




-- Convert from CSV string to table row (converts a single line of a CSV file)
function csv:str2tr (s)
	s = s .. ','		-- ending comma
	local t = {}		-- table to collect fields
	local fieldstart = 1
	repeat
	-- next field is quoted? (start with `"'?)
	if string.find(s, '^"', fieldstart) then
		local a, c
		local i	= fieldstart
		repeat
		-- find closing quote
		a, i, c = string.find(s, '"("?)', i+1)
		until c ~= '"'	-- quote not followed by quote?
		if not i then error('unmatched "') end
		local f = string.sub(s, fieldstart+1, i-1)
		table.insert(t, (string.gsub(f, '""', '"')))
		fieldstart = string.find(s, ',', i) + 1
	else				-- unquoted; find next comma
		local nexti = string.find(s, ',', fieldstart)
		table.insert(t, string.sub(s, fieldstart, nexti-1))
		fieldstart = nexti + 1
	end
	until fieldstart > string.len(s)
	return t
end

--[[
if tonumber(a) ~= nil then
   --it's a number
end;
]]



-- Convert from table row to CSV string
function csv:tr2str (tt)		-- toCSV
	local s = ""
-- ChM 23.02.2014: changed pairs to ipairs 
-- assumption is that fromCSV and toCSV maintain data as ordered array
	for _,p in ipairs(tt) do	
		s = s .. "," .. self:escapeQ(p)
	end
	return string.sub(s, 2)		-- remove first comma
end

function csv:table2str (t)

	local str = ''
	local line = ''
	
	for _,row in ipairs(t) do
		line = self:tr2str(row) .. '\r\n'
		str = str .. line 
	end

	return str
end










function csv:ParseCSVLine (line,sep) 
	local res = {}
	local pos = 1
	sep = sep or ','
	while true do 
		local c = string.sub(line,pos,pos)
		if (c == "") then break end
		if (c == '"') then
			-- quoted value (ignore separator within)
			local txt = ""
			repeat
				local startp,endp = string.find(line,'^%b""',pos)
				txt = txt..string.sub(line,startp+1,endp-1)
				pos = endp + 1
				c = string.sub(line,pos,pos) 
				if (c == '"') then txt = txt..'"' end 
				-- check first char AFTER quoted string, if it is another
				-- quoted string without separator, then append it
				-- this is the way to "escape" the quote char in a quote. example:
				--   value1,"blub""blip""boing",value3  will result in blub"blip"boing  for the middle
			until (c ~= '"')
			table.insert(res,txt)
			assert(c == sep or c == "")
			pos = pos + 1
		else	
			-- no quotes used, just look for the first separator
			local startp,endp = string.find(line,sep,pos)
			if (startp) then 
				table.insert(res,string.sub(line,pos,startp-1))
				pos = endp + 1
			else
				-- no separator found -> use rest of string and terminate
				table.insert(res,string.sub(line,pos))
				break
			end 
		end
	end
	return res
end




function csv:fromFile (fName)

	local t = {}
	local l
	
	if not love.filesystem.getInfo(fName) then return nil end
	
	for line in love.filesystem.lines(fName) do
	
		l = self:str2tr(line)
		table.insert(t, l)
	end

	return t
end

function csv:toFile (fName)

	

	love.filesystem.write(fName, t)
end




return csv