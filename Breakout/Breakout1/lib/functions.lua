-- 4 11 2018




-- debug table
function printT (t, tab)
	
	tab = tab or ''
	if not isTable(t) then
		print('Not table.')
		return
	end
	
	if isEmptyTable(t) then
		print('Empty table')
	end
	
	for k, v in pairs(t) do
		io.write(tab..tostring(k)..': ')
		if isTable(v) then
			io.write('\r\n')
			printT (v, '  ')
			io.write('\r\n')
		else
			io.write(tostring(v)..'\r\n')
		end
--		print(tostring(k), ': ', tostring(v))
	end
end

function isTable (t) return type(t) == 'table' end

function isEmptyTable (t) return next(t) == nil end

-- index val
function arrayMerge2 (t1, t2)

	local l1 = #t1
	local l2 = #t2

	for i = 1, l2 do
		l1 = l1 + 1
		t1[l1] = t2[i]
	end
	
	return t1
end

function arrayMerge (...)
	local arg = {...}
	local arg_len = #arg
	
	local a = arg[1] or {}
	local al = #a
	
	for i = 2, arg_len do
		
		j_len = #arg[i]
		for j = 1, j_len do
			al = al + 1
			a[al] = arg[i][j]
		end
	end
	
	return a
end


-- key val
function tableMerge (t1, t2)
	for k,v in pairs(t2) do
		if type(v) == "table" then
			if type(t1[k] or false) == "table" then
				tableMerge(t1[k] or {}, t2[k] or {})
			else
				t1[k] = v
			end
		else
			t1[k] = v
		end
	end
	return t1
end


-- array has all indexes
function arrayFilter (t, v)

	local res = {}
	local l = #t
	local count = 1
	
	for i = 1, l do
		if t[i] ~= v then res[count] = t[i] end
	end
	
	return res
end

-- array has all indexes
function tableFilter (t, f)

	local res = {}
	local count = 1
	
	for k, v in pairs(t) do
		if v ~= f then res[k] = v end
	end

	return res
end

--[[
	Utility function for slicing tables, a la Python.

	https://stackoverflow.com/questions/24821045/does-lua-have-something-like-pythons-slice
]]
function table.slice(tbl, first, last, step)
	local sliced = {}
	local index = 0
	
	for i = first or 1, last or #tbl, step or 1 do
		index = index + 1
		sliced[index] = tbl[i]
	end
  
	return sliced
end


--[[
assume MARGINS collapse!
atlas	img
x		top
y		left
w		width of quad, no padding or margin
h		height of quad, no padding or margin
n		number of same squares
m		margin in px

]]
function GenerateQuadsX (atlas, x, y, w, h, n, m)
	m = m and m or 0
	local quads = {}
	local aw, ah = atlas:getDimensions()
	
	for i = 1, n do	
		quads[i] = love.graphics.newQuad(x+m, y+m, w, h, aw, ah)
		x = x + m + w
	end
	
	return quads
end

--[[
assume MARGINS collapse!
atlas	img
x		top
y		left
w		width of quad, no padding or margin
h		height of quad, no padding or margin
n		number of same squares
m		margin in px
]]
function GenerateQuadsY (atlas, x, y, w, h, n, m)
	m = m and m or 0
	local quads = {}
	local aw, ah = atlas:getDimensions()
	
	for i = 1, n do	
		quads[i] = love.graphics.newQuad(x+m, y+m, w, h, aw, ah)
		y = y + m + h
	end
	
	return quads
end

-- atlas/texture with multiple sprites, split texture into all quads by simply dividing it evenly.
function GenerateQuads(atlas, tilewidth, tileheight)

	local w, h = atlas:getDimensions()
	local sheetWidth = w / tilewidth
	local sheetHeight = h / tileheight

	local sheetCounter = 1
	local spritesheet = {}
	

	for y = 0, sheetHeight - 1 do
		for x = 0, sheetWidth - 1 do
			spritesheet[sheetCounter] =
				love.graphics.newQuad(x * tilewidth, y * tileheight, tilewidth, tileheight, w, h)
			sheetCounter = sheetCounter + 1
		end
	end

	return spritesheet
end



function collision (a, b)

	if not a or not b then
		print ('collision')
		print(a)
		print(b)
		return false
	end
	-- first, check to see if left edge of either is farther to right
	-- than the right edge of the other
	if a.x > b.x + b.width or b.x > a.x + a.width then
		return false
	end

	-- then check to see if the bottom edge of either is higher than the top
	-- edge of the other
	if a.y > b.y + b.height or b.y > a.y + a.height then
		return false
	end 

	-- if the above aren't true, they're overlapping
	return true
end
