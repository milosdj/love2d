require 'src/include'

--	https://zenpencils.com/comic/chuck/
--	I could only be as good as I could be. whatever my limits are.

--[[
helpState
show all sprites
]]


	local backgroundWidth
	local backgroundHeight


	
-- called just once at the beginning of the game, prep world
function love.load()
	-- s"nearest-neighbor", no filtering of pixels (blurriness), crisp, 2D look
	love.graphics.setDefaultFilter('nearest', 'nearest')
	math.randomseed(os.time())

	love.window.setTitle('Breakout')

	keyDown = {}

	gFonts = {
		small = love.graphics.newFont('font/font.ttf', 8),
		medium = love.graphics.newFont('font/font.ttf', 16),
		large = love.graphics.newFont('font/font.ttf', 32)
	}
	love.graphics.setFont(gFonts['small'])

	gTextures = {
		background = love.graphics.newImage('img/background.png'),
		main = love.graphics.newImage('img/breakout1.png'),
		--arrows = love.graphics.newImage('img/arrows.png'),
		--hearts = love.graphics.newImage('img/hearts.png'),
		particle = love.graphics.newImage('img/particle.png')
	}
	
	backgroundWidth = gTextures['background']:getWidth()
	backgroundHeight = gTextures['background']:getHeight()	
	
	
	push:setupScreen(VIRTUAL_WIDTH, VIRTUAL_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT, {
		vsync = true,
		fullscreen = false,
		resizable = true
	})

	gSounds = {
		paddle_hit = love.audio.newSource('snd/paddle_hit.mp3', 'static'),
		score = love.audio.newSource('snd/score.mp3', 'static'),
		wall_hit = love.audio.newSource('snd/wall_hit.mp3', 'static'),
		confirm = love.audio.newSource('snd/confirm.mp3', 'static'),
		select = love.audio.newSource('snd/select.mp3', 'static'),
		no_select = love.audio.newSource('snd/no-select.mp3', 'static'),
		brick_hit_1 = love.audio.newSource('snd/brick-hit-1.mp3', 'static'),
		brick_hit_2 = love.audio.newSource('snd/brick-hit-2.mp3', 'static'),
		hurt = love.audio.newSource('snd/hurt.mp3', 'static'),
		victory = love.audio.newSource('snd/victory.mp3', 'static'),
		recover = love.audio.newSource('snd/recover.mp3', 'static'),
		high_score = love.audio.newSource('snd/high_score.mp3', 'static'),
		pause = love.audio.newSource('snd/pause.mp3', 'static'),
		music = love.audio.newSource('snd/music.mp3', 'static')
	}

	gFrames = {
		paddles = GenerateQuadsPaddles(gTextures['main']),
		balls = GenerateQuadsBalls(gTextures['main']),
		bricks = GenerateQuadsBricks(gTextures['main']),
	--	locked = GenerateQuadsLockedBricks(gTextures['main']),
		hearts = GenerateQuadsHearts(gTextures['main']),		-- GenerateQuads(gTextures['hearts'], 8, 8),
		arrows = GenerateQuadsArrows(gTextures['main']),		-- GenerateQuads(gTextures['arrows'], 24, 24),
		powerups = GenerateQuadsPowerups(gTextures['main']),
	}
	
	-- our current game state can be any of the following:
	-- 1. 'start' (beginning of game, press Enter)
	-- 2. 'paddle-select' (choose color of paddle)
	-- 3. 'serve' (waiting on a key press to serve ball)
	-- 4. 'play' (ball is in play, bouncing between paddles)
	-- 5. 'victory' (current level is over, with a victory jingle)
	-- 6. 'gameOver' (player has lost; display score and allow restart)
	-- 7. 'hiScore' display scores
	gStateMachine = StateMachine {
		--start = function() return StartState() end
		start = StartState,
		paddleSelect = PaddleSelectState,
		serve = ServeState,
		play = PlayState,
		victory = VictoryState,
		gameOver = GameOverState,
		hiScore = HiScoreState,
		enterHiScore = EnterHiScoreState,
		help = HelpState,
	}
--	print('main')
	O.hiScores = HiScoreState:loadHiScores()
--	print(O.hiScores)
	gStateMachine:change('start')

	gSounds['music']:setLooping(true)
	if O.music then
		gSounds['music']:play()
	end
end

function love.resize(w, h)
	push:resize(w, h)
end

-- called every frame, deltaTime
function love.update(dt)
	gStateMachine:update(dt)
end

function love.keypressed (k)
	keyDown[k] = true
end

function love.keyreleased (k)
	keyDown[k] = false
	
	if k == 'm' or k == 'M' then
		O.music = not O.music
		if O.music then
			gSounds['music']:play()
		else
			gSounds['music']:stop()
		end
	elseif k == 'f' then
		O.fps = not O.fps
	elseif k == 'n' then
		O.sfx = not O.sfx
	end
end


-- called each frame after update
function love.draw()
push:start()

	-- background should be drawn regardless of state, scaled to fit our virtual resolution
	love.graphics.draw(gTextures['background'], 
		0, 0,	-- draw at coordinates 0, 0
		0,		-- no rotation
		-- scale factors on X and Y axis so it fills the screen
		VIRTUAL_WIDTH / (backgroundWidth - 1), VIRTUAL_HEIGHT / (backgroundHeight - 1))
	
	gStateMachine:render()
	
	if O.fps then displayFPS() end
	
push:finish()
end

