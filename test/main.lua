
WINDOW_WIDTH = 1280
WINDOW_HEIGHT = 720

img = love.graphics.newImage('img/pipe.png')

iW = img:getWidth()
iH = img:getHeight()

function love.load()

	love.window.setTitle('Test')
	love.graphics.setDefaultFilter('nearest', 'nearest')



    love.window.setMode(WINDOW_WIDTH, WINDOW_HEIGHT, {
        fullscreen = false,
        resizable = true,
        vsync = true
    })
	
	
end


function love.update(dt)

end


function love.draw()

	love.graphics.clear(1,1,1,1)
	love.graphics.printf('radi', 0, 0, WINDOW_WIDTH, 'center')

	love.graphics.draw(img, 100, 200)
	love.graphics.draw(img, 300, 200, math.pi)
	
	-- 0 offset
	love.graphics.draw(img, 500, 200, 0, 1, -1, 0, iH)

end