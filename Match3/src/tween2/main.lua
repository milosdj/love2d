--[[
    GD50
    tween2

    Example used to showcase a simple way of "tweening" (interpolating) some value
    over a period of time, in this case by moving Flappy Bird across the screen,
    horizontally. This example instantiates a large number of birds all moving at
    different rates to show a slightly better example than before but uses
    Timer.tween to do it; it also tweens their opacity.
]]

push = require 'push'
Timer = require 'knife.timer'

VIRTUAL_WIDTH = 384
VIRTUAL_HEIGHT = 216

WINDOW_WIDTH = 1280
WINDOW_HEIGHT = 720

-- longest possible movement duration
TIMER_MAX = 10

function love.load()
    flappySprite = love.graphics.newImage('flappy.png')

    -- table of birds with random movement rates and Y positions
    birds = {}

    -- create 1000 random birds
	-- 61000 ptica 35fps
	
	-- 63  33fps
    for i = 1, 31000 do
        table.insert(birds, {
            -- all start at left side
            x = 0,

            -- random Y position within screen boundaries
            y = math.random(VIRTUAL_HEIGHT - 24),

            -- random rate between half a second and our max, floating point
            -- math.random() by itself will generate a random float between 0 and 1,
            -- so we add that to math.random(max) to get a number between 0 and 10,
            -- floating-point
            rate = math.random() + math.random(TIMER_MAX - 1),

            -- start with an opacity of 0 and fade to 255 over duration as well
            opacity = 0
        })
    end

    -- end X position for our interpolations
    endX = VIRTUAL_WIDTH - flappySprite:getWidth()

	local len = #birds
    -- iterate over all birds and tween to the endX location
    for i=1, len  do
		local bird = birds[i]
        Timer.tween(bird.rate, {
            -- tween bird's X to endX over bird.rate seconds
            [bird] = { x = endX, opacity = 1 }
        })
    end

    love.graphics.setDefaultFilter('nearest', 'nearest')

    push:setupScreen(VIRTUAL_WIDTH, VIRTUAL_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT, {
        fullscreen = false,
        vsync = true,
        resizable = true
    })
end

function love.resize(w, h)
    push:resize(w, h)
end

function love.keypressed(key)
    if key == 'escape' then
        love.event.quit()
    end
end

function love.update(dt)
    Timer.update(dt)
end

function love.draw()
    push:start()
	love.graphics.print('FPS: ' .. love.timer.getFPS(), 5, 5)

    -- iterate over bird table for drawing
	local len = #birds
    for i=1, len do
		local bird = birds[i]
        love.graphics.setColor(1, 1, 1, bird.opacity)
        love.graphics.draw(flappySprite, bird.x, bird.y)
    end

    push:finish()
end