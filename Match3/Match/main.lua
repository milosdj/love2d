require 'src/include'

-- physical screen dimensions
WINDOW_WIDTH = 1280
WINDOW_HEIGHT = 720

-- virtual resolution dimensions
VIRTUAL_WIDTH = 512
VIRTUAL_HEIGHT = 288




function love.load()
	love.graphics.setDefaultFilter('nearest', 'nearest')
	love.window.setTitle('Match 3')
	math.randomseed(os.time())

	keyDown = {}
	push:setupScreen(VIRTUAL_WIDTH, VIRTUAL_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT, {
		vsync = true,
		fullscreen = false,
		resizable = true
	})

	gSounds['music']:setLooping(true)
	if O.music then gSounds['music']:play() end

	-- initialize state machine with all state-returning functions
	gStateMachine = StateMachine {
		start = StartState,
		beginGame = BeginGameState,
		play = PlayState,
		gameOver = GameOverState,
	}
	gStateMachine:change('start')

	
	-- keep track of scrolling our background on the X axis
	backgroundX = 0
	backgroundScrollSpeed = 80

end

function love.resize(w, h)
	push:resize(w, h)
end

function love.keypressed(key)
	keyDown[key] = true
end

function love.keyreleased (k)
	keyDown[k] = false
end



function love.update(dt)
	-- scroll background, used across all states
	backgroundX = backgroundX - backgroundScrollSpeed * dt
	-- if we've scrolled the entire image, reset it to 0
	if backgroundX <= -1024 + VIRTUAL_WIDTH - 4 + 51 then backgroundX = 0 end

	gStateMachine:update(dt)
end

function love.draw()
	push:start()

	-- scrolling background drawn behind every state
	love.graphics.draw(gTextures['background'], backgroundX, 0)
	
	gStateMachine:render()
	push:finish()
end