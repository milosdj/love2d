Class = require 'lib/class'

push = require 'lib/push'

-- used for timers and tweening
Timer = require 'lib/knife.timer'




-- our own code

-- utility
require 'src/StateMachine'
require 'src/Util'

-- game pieces
require 'src/Board'
require 'src/Tile'

-- game states
require 'src/states/BaseState'
require 'src/states/BeginGameState'
require 'src/states/GameOverState'
require 'src/states/PlayState'
require 'src/states/StartState'

gSounds = {
	music = love.audio.newSource('audio/music3.mp3', 'static'),
	select = love.audio.newSource('audio/select.mp3', 'static'),
	error = love.audio.newSource('audio/error.mp3', 'static'),
	match = love.audio.newSource('audio/match.mp3', 'static'),
	clock = love.audio.newSource('audio/clock.mp3', 'static'),
	game_over = love.audio.newSource('audio/game-over.mp3', 'static'),
	next_level = love.audio.newSource('audio/next-level.mp3', 'static')
}

gTextures = {
	main = love.graphics.newImage('img/match3.png'),
	background = love.graphics.newImage('img/background.png')
}

	-- table of Quads
gFrames = {
	-- divided into sets for each tile type in this game, instead of one large
	tiles = GenerateTileQuads(gTextures['main'])
}

gFonts = {
	small = love.graphics.newFont('img/font.ttf', 8),
	medium = love.graphics.newFont('img/font.ttf', 16),
	large = love.graphics.newFont('img/font.ttf', 32)
}

O = {}
O['music'] = false
O['sfx'] = true