O = Class{}


O.music = false
O.sfx = true

O.level = 1
O.board = {}


function O:init ()
	self:reset()
end

function O:reset ()
	self.level = 1
	self.board = {}
	
	
	self.music = false
	self.sfx = true
end
